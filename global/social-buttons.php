<div class="tps-social-buttons">
    <a href="<?php echo defined( 'TPS_FACEBOOK_URL' ) ? TPS_FACEBOOK_URL : '#'; ?>" class="tps-btn-social tps-btn-facebook" target="_blank" title="<?php _e( 'Follow us on Facebook', 'tps' ); ?>">
        <i class="fa fa-facebook fa-fw fa-lg"></i>
    </a>
    <a href="<?php echo defined( 'TPS_TWITTER_URL' ) ? TPS_TWITTER_URL : '#'; ?>" class="tps-btn-social tps-btn-twitter" target="_blank" title="<?php _e( 'Follow us on Twitter', 'tps' ); ?>">
        <i class="fa fa-twitter fa-fw fa-lg"></i>
    </a>
    <a href="<?php echo defined( 'TPS_GOOGLE_PLUS_URL' ) ? TPS_GOOGLE_PLUS_URL : '#'; ?>" class="tps-btn-social tps-btn-google-plus" target="_blank" title="<?php _e( 'Follow us on Google Plus', 'tps' ); ?>">
        <i class="fa fa-google-plus fa-fw fa-lg"></i>
    </a>
    <a href="<?php echo defined( 'TPS_YOUTUBE_URL' ) ? TPS_YOUTUBE_URL : '#'; ?>" class="tps-btn-social tps-btn-youtube" target="_blank" title="<?php _e( 'Follow us on Youtube', 'tps' ); ?>">
        <i class="fa fa-youtube fa-fw fa-lg"></i>
    </a>
    <a href="<?php echo defined( 'TPS_INSTAGRAM_URL' ) ? TPS_INSTAGRAM_URL : '#'; ?>" class="tps-btn-social tps-btn-instagram" target="_blank" title="<?php _e( 'Follow us on Instagram', 'tps' ); ?>">
        <i class="fa fa-instagram fa-fw fa-lg"></i>
    </a>
    <a href="<?php echo defined( 'TPS_PINTEREST_URL' ) ? TPS_PINTEREST_URL : '#'; ?>" class="tps-btn-social tps-btn-pinterest" target="_blank" title="<?php _e( 'Follow us on Pinterestk', 'tps' ); ?>">
        <i class="fa fa-pinterest fa-fw fa-lg"></i>
    </a>
</div>