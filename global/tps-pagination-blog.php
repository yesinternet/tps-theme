<nav class="tps-pagination" aria-label="Pagination">
	<?php
	$pagination = paginate_links( array(
		'prev_text' => '&laquo;',
		'next_text' => '&raquo;',
		'type'      => 'array',
		'end_size'  => 3,
		'mid_size'  => 3
	) );
	?>
    <ul class="pagination page-numbers">
		<?php
		if ( ! empty( $pagination ) ):
			foreach ( $pagination as $page ):
				$search = 'current';
				$current = strpos( $page, $search );
				?>
                <li class="<?php echo $current ? 'active' : ''; ?>">
					<?php echo $page; ?>
                </li>
			<?php
			endforeach;
		endif;
		?>
    </ul>
</nav>