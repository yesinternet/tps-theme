<?php

global $product;

$meta_array = get_post_meta( $product->get_id() );

$has_oembed_meta = preg_filter( '/^_oembed(.*)/', '$1', array_keys( $meta_array ) );

$has_video = ! empty( $has_oembed_meta ) ? true : false;

$recommended_page = get_page_by_path( TPS_RECOMMENDED_PAGE );
$recommended_link = get_page_link( $recommended_page );

$onsale_page = get_page_by_path( TPS_ONSALE_PAGE );
$onsale_link = get_page_link( $onsale_page );

ob_start();

do_action( 'tps_product_properties_icons' );

$actions = ob_get_clean();

if ( $has_video || $product->is_featured() || $product->is_on_sale() || ! empty( $actions ) ):
	?>

    <div class="tps-product-icons">

		<?php if ( $has_video ) : ?>
            <span class="btn tps-btn tps-btn-video" title="<?php _e( 'Video', 'tps' ); ?>" data-toggle="tooltip" data-placement="bottom">
        <i class="fa fa-play" aria-hidden="true"></i> <span><?php _e( 'Video', 'tps' ); ?></span>
    </span>
		<?php endif; ?>

		<?php if ( $product->is_featured() ) : ?>
			<?php if ( $recommended_page )
				echo '<a href="' . $recommended_link . '" title="' . __( 'Click to see the recommended products', 'tps' ) . '" data-toggle="tooltip" data-placement="bottom">' ?>
            <span class="btn tps-btn tps-btn-featured"><i class="fa fa-star" aria-hidden="true"></i> <span><?php _e( 'Recommended', 'tps' ); ?></span></span>
			<?php if ( $recommended_page )
				echo '</a>' ?>
		<?php endif; ?>

		<?php if ( $product->is_on_sale() ) : ?>
			<?php if ( $onsale_page )
				echo '<a href="' . $onsale_link . '" title="' . __( 'Click to see the on sale products', 'tps' ) . '" data-toggle="tooltip" data-placement="bottom">' ?>
            <span class="btn tps-btn tps-btn-onsale"><i class="fa fa-percent" aria-hidden="true"></i> <span><?php _e( 'On Sale', 'tps' ); ?></span></span>
			<?php if ( $onsale_page )
				echo '</a>' ?>
		<?php endif; ?>

		<?php echo $actions; ?>

    </div>

<?php endif; ?>