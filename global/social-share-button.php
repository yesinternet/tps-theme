<?php

global $tps_post_social_url;
global $tps_post_pinterest_url;
global $tps_post_social_title;

?>

<span class="tps-btn-share-container dropup">
    <div class="btn tps-btn tps-btn-share dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-share-alt" aria-hidden="true"></i> <span class="hidden-xs"><?php _e( 'Share', 'tps' ); ?></span>
    </div>
    <ul class="dropdown-menu">
        <li><a href="//www.facebook.com/sharer.php?u=<?php echo $tps_post_social_url; ?>" target="_blank" title="<?php _e( 'Share it on Facebook', 'tps' ); ?>"><?php _e( 'Facebook', 'tps' ); ?></a></li>
        <li><a href="//twitter.com/home?status=<?php echo $tps_post_social_url; ?>" target="_blank" title="<?php _e( 'Share it on Twitter', 'tps' ); ?>"><?php _e( 'Twitter', 'tps' ); ?></a></li>
        <li><a href="<?php echo $tps_post_pinterest_url; ?>" target="_blank" title="<?php _e( 'Share it on Pinterest', 'tps' ); ?>"><?php _e( 'Pinterest', 'tps' ); ?></a></li>
        <li><a href="//plus.google.com/share?url=<?php echo $tps_post_social_url; ?>" target="_blank" title="<?php _e( 'Share it on Google Plus', 'tps' ); ?>"><?php _e( 'Google+', 'tps' ); ?></a></li>
        <li><a href="mailto:?subject=<?php echo $tps_post_social_title; ?>&body=<?php echo $tps_post_social_url; ?>" title="<?php _e( 'Share it with Email', 'tps' ); ?>"><?php _e( 'Email', 'tps' ); ?></a></li>
    </ul>
</span>
