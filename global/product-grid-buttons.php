<?php
global $product;

global $tps_post_social_url;
global $tps_post_pinterest_url;
global $tps_post_social_title;

$tps_post_id = $product->get_id();

$tps_product_url = urlencode( get_the_permalink( $tps_post_id ) );

$tps_product_image_url = urlencode( get_the_post_thumbnail_url( get_post( $product->get_id() ), 'shop_catalog' ) );

$tps_promo_heading                 = get_post_meta( $tps_post_id, '_tps_promo_text', true );
$tps_product_pinterest_description = ! empty( $tps_promo_heading ) ? $tps_promo_heading : apply_filters( 'woocommerce_short_description', $product->get_short_description() );
$tps_product_pinterest_description = urlencode( wp_strip_all_tags( $tps_product_pinterest_description, true ) );
$tps_product_pinterest_url         = "//pinterest.com/pin/create/button/?url=" . $tps_product_url . "&media=" . $tps_product_image_url . "&description=" . $tps_product_pinterest_description;

$tps_post_social_url    = $tps_product_url;
$tps_post_pinterest_url = $tps_product_pinterest_url;
$tps_post_social_title  = get_the_title( $tps_post_id );
?>

<div class="tps-product-buttons">

    <div class="tps-product-buttons-left">
		<?php get_template_part( "global/social-share-button" ); ?>
    </div>

    <div class="tps-product-buttons-right">
		<?php do_action( "tps-product-wishlist-button" ); ?>
    </div>

</div>