<?php

global $tps_post_social_url;
global $tps_post_pinterest_url;
global $tps_post_social_title;

?>

<div class="tps-post-share">
    <a href="//www.facebook.com/sharer.php?u=<?php echo $tps_post_social_url; ?>" class="tps-btn-social tps-btn-facebook" target="_blank" title="<?php _e( 'Share it on Facebook', 'tps' ); ?>">
        <i class="fa fa-facebook fa-fw fa-lg"></i>
    </a>
    <a href="//twitter.com/home?status=<?php echo $tps_post_social_url; ?>" class="tps-btn-social tps-btn-twitter" target="_blank" title="<?php _e( 'Share it on Twitter', 'tps' ); ?>">
        <i class="fa fa-twitter fa-fw fa-lg"></i>
    </a>
    <a href="<?php echo $tps_post_pinterest_url; ?>" class="tps-btn-social tps-btn-pinterest" target="_blank" title="<?php _e( 'Share it on Pinterest', 'tps' ); ?>">
        <i class="fa fa-pinterest fa-fw fa-lg"></i>
    </a>
    <a href="//plus.google.com/share?url=<?php echo $tps_post_social_url; ?>" class="tps-btn-social tps-btn-google-plus" target="_blank" title="<?php _e( 'Share it on Google Plus', 'tps' ); ?>">
        <i class="fa fa-google-plus fa-fw fa-lg"></i>
    </a>
    <a href="mailto:?subject=<?php echo $tps_post_social_title; ?>&body=<?php echo $tps_post_social_url; ?>" class="tps-btn-social tps-btn-email" title="<?php _e( 'Share it with Email', 'tps' ); ?>">
        <i class="fa fa-envelope-o fa-fw fa-lg"></i>
    </a>
</div>