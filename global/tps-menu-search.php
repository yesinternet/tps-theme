<div class="tps-menu-search navbar-right ">
    <div class="tps-menu-search-btn btn"><i class="fa fa-search" title="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>"></i></div>
    <div class="tps-menu-search-field">
        <div class="container">
            <form role="search" method="get" class="search-form navbar-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s"
                       title="<?php _ex( 'Search for:', 'label', 'tps' ); ?>" data-swplive="true"/>
                <button type="submit" class="search-submit btn btn-default navbar-right"><i class="fa fa-search" title="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>"></i></button>
                <input type="hidden" name="post_type" value="product"/>
            </form>
        </div>
    </div>
</div>