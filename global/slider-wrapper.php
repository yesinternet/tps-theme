<?php

global $tps_slider;
global $general_slider_script;
global $tps_index;
global $tps_slider_index;

wp_enqueue_script( 'tps-owl-carousel-js', get_template_directory_uri() . '/js/owl.carousel.js', array(), '2.2.0', true );
wp_enqueue_script( 'tps-slider-js', get_template_directory_uri() . '/js/tps-slider.js', array( "tps-owl-carousel-js" ), TPS_THEME_VERSION, true );

if ( ! empty( $tps_slider['slides'] ) ):
	$tps_slider_index ++;

	$wrapper_id    = 'tps_slider_' . get_the_ID() . '_' . $tps_slider_index;
	$items         = $tps_slider['items'];
	$loop          = $tps_slider['loop'] == 'true' ? 'true' : 'false';
	$center        = $tps_slider['center'] == 'true' ? 'true' : 'false';
	$stagePadding  = $tps_slider['stagePadding'];
	$nav           = $tps_slider['nav'] == 'true' ? 'true' : 'false';
	$navText       = '[' . implode( ",", $tps_slider['navText'] ) . ']';
	$autoplay      = $tps_slider['autoplay'] == 'true' ? 'true' : 'false';
	$lazyload      = $tps_slider['lazyload'] == 'true' ? 'true' : 'false';
	$fullwidth     = $tps_slider['fullwidth'] == 'true' ? true : false;
	$fullheight    = $tps_slider['fullheight'] == 'true' ? 'tps-image-full-height' : '';
	$is_fullheight = $tps_slider['fullheight'] == 'true' ? true : false;
	$autoHeight    = ! $is_fullheight ? 'true' : 'false';

	$mousedrag = 'true';
	$touchdrag = 'true';
	if ( $tps_index <= 1 ) {
		$loop      = 'false';
		$nav       = 'false';
		$mousedrag = 'false';
		$touchdrag = 'false';
	}

	ob_start();
	?>
    <div class="tps-slider-wrapper <?php echo $fullheight; ?>">
        <div class="owl-carousel <?php echo $wrapper_id; ?>">
			<?php echo $tps_slider['slides']; ?>
        </div>
		<?php if ( $is_fullheight ): ?>
            <div class="tps-image-full-height-arrow"><a href="#" class="tps-scrollDown"><i class="fa fa-chevron-down fa-3x tps-bounce"></i></a></div>
		<?php endif; ?>
    </div>

	<?php
	$return = ob_get_clean();

	$return = ! $fullwidth ? '<div class="container">' . $return . '</div>' : $return;

	echo $return;

	$script = "
        jQuery(function($){
            var element = $('.$wrapper_id');
            tps_slider(element, {
                items: $items,
                loop: $loop,
                center: $center,
                stagePadding: $stagePadding,
                nav: $nav,
                navText: $navText,
                autoplay: $autoplay,
                lazyLoad: $lazyload,
                autoHeight: $autoHeight,
                responsiveRefreshRate: 50,
                mouseDrag: $mousedrag,
                touchDrag: $touchdrag
            });
        });
    ";

//    if(!empty($tps_slider['id']) || !$general_slider_script) {
	wp_add_inline_script( 'tps-slider-js', $script );
//    }

//    $general_slider_script = !empty($tps_slider['id']) ? $general_slider_script : 1;
	?>

<?php endif; ?>