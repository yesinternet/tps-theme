<?php
$current_user_id = get_current_user_id();
$url             = get_avatar_url( $current_user_id, array( 'size' => 40 ) );

if ( defined( 'APSL_SETTINGS' ) ) {
	$options = get_option( APSL_SETTINGS );
	if ( ! empty( $current_user_id ) ) {
//        //Override current avatar ?
//        $override_avatar = true;
		//Read the avatar
		$user_meta_thumbnail = get_user_meta( $current_user_id, 'deuimage', true );

		if ( $options['apsl_user_avatar_options'] == 'social' ) {
			$user_picture = ( ! empty( $user_meta_thumbnail ) ? $user_meta_thumbnail : '' );
			//Avatar found?
			if ( $user_picture !== false and strlen( trim( $user_picture ) ) > 0 ) {
				$url = $user_picture;
			}
		}
	}
}

?>
<li id="tps_menu_logged_in" class="tps-menu-logged-in">
    <a href="#" title="<?php _e( "Profile", "tps" ); ?>" class="dropdown-toggle"
       data-toggle="dropdown" data-hover="dropdown" data-delay="1" aria-haspopup="true" aria-expanded="false"
       style="background-image: url(<?php echo $url; ?>)">

        <span class="visible-xs-inline"><?php _e( "Profile", "tps" ); ?> <i class="fa fa-chevron-down"></i></span>
    </a>
    <ul class="dropdown-menu" role="menu">
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
            <li class="<?php //echo wc_get_account_menu_item_classes( $endpoint ); ?>">
                <a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
            </li>
		<?php endforeach; ?>
    </ul>
</li>