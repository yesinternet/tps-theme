<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tps
 */

if ( ! is_active_sidebar( 'shop' ) ) {
	return;
}
?>

<aside id="secondary" class="col-sm-4 widget-area" role="complementary">
	<?php dynamic_sidebar( 'shop' ); ?>
</aside><!-- #secondary -->
