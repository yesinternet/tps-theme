<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tps
 */

if ( !is_front_page() /*&& !is_page_template('page-templates/main-page.php') && !is_page() && !is_product_category() && !is_search()*/) {
    get_template_part( 'global/tps-breadcrumbs' );
}
$tps_about_page = defined('TPS_ABOUT_PAGE') ? TPS_ABOUT_PAGE : '#';
$tps_sell_page = defined('TPS_SELL_PAGE') ? TPS_SELL_PAGE : '#';
$tps_press_page = defined('TPS_PRESS_PAGE') ? TPS_PRESS_PAGE : '#';
$tps_contact_page = defined('TPS_CONTACT_PAGE') ? TPS_CONTACT_PAGE : '#';
$tps_profile_page = defined('TPS_PROFILE_PAGE') ? TPS_PROFILE_PAGE : '#';
$tps_recommended_page = defined('TPS_RECOMMENDED_PAGE') ? TPS_RECOMMENDED_PAGE : '#';
$tps_onsale_page = defined('TPS_ONSALE_PAGE') ? TPS_ONSALE_PAGE : '#';
$tps_terms_page = defined('TPS_TERMS_PAGE') ? TPS_TERMS_PAGE : '#';
$tps_privacy_page = defined('TPS_PRIVACY_PAGE') ? TPS_PRIVACY_PAGE : '#';
?>
	<footer id="colophon" class="site-footer" role="contentinfo">
        <div class="container">
            <div class="row">
                <div class="site-footer-inner col-sm-12">
                    <div class="tps-footer-top row">
                        <div class="tps-footer-block col-sm-5">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="tps-footer-logo">
                                <img src="<?php header_image(); ?>" class="img-responsive" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
                            </a>
                            <div class="text-justify">
                                <?php _e('A go to place for curated, innovative, amazing and hi tech new products that parents will love to have.', 'tps'); ?>
                            </div>
                            <hr>
                            <div class="tps-footer-social">
                                <span><?php _e('Follow Us:', 'tps'); ?></span>
                                <?php get_template_part('global/social-buttons'); ?>
                            </div>
                            <hr class="visible-xs-block">
                        </div>
                        <div class="tps-footer-block col-sm-offset-1 col-sm-3 col-xs-6">
                            <h3><?php _e('About', 'tps'); ?></h3>
                            <div><a href="<?php echo home_url($tps_about_page); ?>"><?php _e('Our Story', 'tps'); ?></a></div>
                            <div><a href="<?php echo home_url($tps_sell_page); ?>"><?php _e('Sell with us', 'tps'); ?></a></div>
                            <div><a href="<?php echo home_url($tps_press_page); ?>"><?php _e('Press & Media', 'tps'); ?></a></div>
                            <div><a href="<?php echo home_url($tps_contact_page); ?>"><?php _e('Contact Us', 'tps'); ?></a></div>
                        </div>
                        <div class="tps-footer-block col-sm-3 col-xs-6">
                            <h3><?php _e('Explore', 'tps'); ?></h3>
                            <div><a href="<?php echo home_url($tps_profile_page); ?>"><?php _e('Sign in/Profile', 'tps'); ?></a></div>
                            <div><a href="<?php echo home_url('#'); ?>" class="tps-footer-search"><?php _e('Search', 'tps'); ?></a></div>
                            <div><a href="<?php echo home_url($tps_recommended_page); ?>"><?php _e('Recommended', 'tps'); ?></a></div>
                            <div><a href="<?php echo home_url($tps_onsale_page); ?>"><?php _e('On Sale', 'tps'); ?></a></div>
                        </div>
                    </div>
                    <hr>
                    <div class="site-info tps-footer-bottom text-center">
                        <span><?php _e('&copy; 2016 - The Parents Shop, All Rights Reserved', 'tps');?></span>
                        <span class="hidden-xs"> - </span>
                        <span><a href="<?php echo home_url($tps_terms_page); ?>"><?php _e('Terms & Conditions', 'tps');?></a></span>
                        <span class="hidden-xs"> | </span>
                        <span><a href="<?php echo home_url($tps_privacy_page); ?>"><?php _e('Privacy Policy', 'tps');?></a></span>
                    </div><!-- .site-info -->
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
