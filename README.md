# TPS Theme

## How to setup
Create a file named config.php in the root directory of the plugin, according to the provided config-sample.php.

## How to add Pricing packages
In the config file add in $tps_pricing_plan_ids array the product ids.

## How to add Team members
In the config file add in $tps_our_team array the member details according to the sample.

## How to change site logo
* For the main logo open the Customizer and add the logo in the "Header Image" setting.
* For the mobile logo open the Customizer and add the logo in the "Site Identity" setting.

## Image sizes
1. tps_image_xlarge : 1920 x 1080
2. tps_image_large : 1920 x 800
3. tps_image_medium : 1366 x 768
4. tps_image_blog_grid : 400 x 225

## Tps-Shortcodes
* tps_product_category : List products in a category shortcode.

* tps_product_categories : List all (or limited) product categories.

* tps_featured_products : Outputs featured products.

* tps_sale_products : List all products on sale.

* tps_recent_products : Recent Products shortcode.

* tps_products : List multiple products shortcode.

* tps_product_cat_products_slider : Slider with the products in a category.

* tps_homepage_products_primary_slider : Slider with the products that "Homepage Primary Slider" option is selected in "Product Visibility" setting.

* tps_homepage_products_secondary_slider : Slider with the products that "Homepage Secondary Slider" option is selected in "Product Visibility" setting.

* tps_homepage_product_cat_slider : Slider with the product categories that "Homepage Category Slider" option is selected in "Visibility" setting.

* tps_sell : "Sell with us" page shortcode.

* tps_about : "About us" page shortcode.

* tps_press : "Press & Media" page shortcode.

* tps_contact : "Contact us" page shortcode.

* tps_social_icons : Social links with icons for the current post.

* tps_posts_categories : List of the blog categories.

* tps_faq : F.A.Q. shortcode.