<?php 

global $post;

if ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent != 0) ) {
	wp_redirect(get_permalink($post->post_parent), 301); // permanent redirect to post/page where image or document was uploaded
	exit;
} elseif ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent < 1) ) {   // for some reason it doesnt works checking for 0, so checking lower than 1 instead...
	wp_redirect(get_bloginfo('url'), 301); // temp redirect to home for image or document not associated to any post/page
	exit;       
}