<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package tps
 */

get_header(); ?>

    <div id="primary" class="content-area single-wrapper">
        <main id="main" class="site-main container" role="main">
            <div class="row">
                <div class="col-sm-12">
                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', get_post_format() );

                    the_post_navigation( array('screen_reader_text' => __( 'Read also', 'tps' )) );
                ?>
                    <div class="clearfix"></div>
                <?php

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
                ?>
                </div>
            </div><!-- .row -->
        </main><!-- #main -->
    </div><!-- #primary -->

<?php

get_footer();
