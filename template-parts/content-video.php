<?php

global $tps_video;

$tps_video = false;
$content   = apply_filters( 'the_content', get_the_content() );
$video     = false;

// Only get video from the content if a playlist isn't present.
if ( false === strpos( $content, 'wp-playlist-script' ) ) {
	$video = get_media_embedded_in_content( $content, array( 'video', 'object', 'embed', 'iframe' ) );
}
$tps_video = $video;

get_template_part( 'template-parts/content' );


