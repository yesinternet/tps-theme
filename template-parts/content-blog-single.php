<?php
ob_start();
?>
<div class="tps-blog-post-header">
    <div class="tps-blog-post-categories"><?php echo tps_post_categories(); ?></div>
    <div class="tps-blog-post-title h1"><?php echo the_title(); ?></div>
	<?php if ( 'post' === get_post_type() ) : ?>
        <span class="tps-author"><?php echo tps_post_author_html(); ?></span>
        <span class="tps-post-date"><?php echo __( ' on ' ) . tps_post_date_html(); ?></span>
	<?php endif; ?>
	<?php do_shortcode( '[tps_social_icons]' ); ?>
</div>

<?php
$header = ob_get_clean();

$tps_post_thumb = get_the_post_thumbnail( null, 'tps_image_large' );

if ( ! empty( $tps_post_thumb ) ): ?>

    <div class="tps-blog-single-image tps-image-medium-height">
		<?php echo $tps_post_thumb; ?>
        <div class="tps-image-text-container">
			<?php echo $header; ?>
        </div>
    </div>

<?php else: ?>

    <header>
		<?php echo $header; ?>
    </header><!-- header -->

<?php endif; ?>

<div class="tps-blog-post-content">
	<?php
	the_content( sprintf(
	/* translators: %s: Name of current post. */
		wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'tps' ), array( 'span' => array( 'class' => array() ) ) ),
		the_title( '<span class="screen-reader-text">"', '"</span>', false )
	) );

	$tps_post_source = get_post_meta( get_the_ID(), '_tps_post_source', true );
	if ( ! empty( $tps_post_source ) ):

		?>

        <a href="<?php echo $tps_post_source; ?>" target="_blank" rel="nofollow" title="<?php _e( 'Click to navigate to the source webpage of this post', 'tps' ); ?>">
			<?php _e( 'Source', 'tps' ); ?>
        </a>

	<?php

	endif;

	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tps' ),
		'after'  => '</div>',
	) );
	?>
</div><!-- .tps-blog-post-content -->

