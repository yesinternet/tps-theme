<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tps
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="homepage-content">
		<?php
		echo do_shortcode( '[tps_homepage_products_primary_slider fullwidth=true fullheight=true]' );

		echo do_shortcode( '[tps_featured_products columns="3" per_page="6"]' );

		echo do_shortcode( '[tps_homepage_product_cat_slider fullwidth=true fullheight=true slider_button_text="Explore the Category"]' );

		echo '<h3 class="tps-shortcode-grid-header text-center">' . __( "On Sale", 'tps' ) . '</h3>';
		echo do_shortcode( '[tps_sale_products columns="3" per_page="12"]' );

		echo do_shortcode( '[tps_homepage_products_secondary_slider fullwidth=true fullheight=true]' );

		echo '<h3 class="tps-shortcode-grid-header text-center">' . __( "Latest products", 'tps' ) . '</h3>';
		echo do_shortcode( '[tps_recent_products columns="3" per_page="12"]' );
		?>
		<?php
		the_content();
		?>
    </div><!-- .entry-content -->

</article><!-- #post-## -->