<?php

global $tps_video;

ob_start();
if ( ! empty( $tps_video ) ) :
	foreach ( $tps_video as $video_html ) {
		$tps_post_video = $video_html;
		break;
	}
	$tps_video = false;
	?>
    <div class="tps-blog-item-video">
		<?php echo ! empty( $tps_post_video ) ? $tps_post_video : ''; ?>
    </div>
<?php
else:
	$tps_post_thumb = get_the_post_thumbnail( null, 'tps_image_blog_grid' );
	if ( empty( $tps_post_thumb ) ) {
		$tps_post_thumb = '<div class="tps-blog-item-no-image"><i class="fa fa-2x fa-picture-o"></i></div>';
	}
	?>
    <div class="tps-blog-item-image">
        <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
			<?php
			echo ! empty( $tps_post_thumb ) ? $tps_post_thumb : '';
			?>
            <div class="tps-overlay"></div>
        </a>
    </div>
<?php

endif;

do_shortcode( '[tps_social_icons is_button=1]' );

$tps_post_image = ob_get_clean();

?>

<div class="tps-blog-item-container">
	<?php echo $tps_post_image; ?>
    <div class="tps-blog-item-info">
        <h3 class="tps-blog-item-title">
            <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php echo the_title(); ?></a>
        </h3>
        <div class="tps-blog-item-categories">
			<?php the_category( ', ' ); ?>
        </div>
        <div class="tps-blog-item-author">
            <span class="tps-author">
                <?php echo tps_post_author_html(); ?>
            </span>

            <span class="tps-post-date">
                <?php echo tps_post_date_html(); ?>
            </span>

        </div>
    </div>
</div>