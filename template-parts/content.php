<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tps
 */

$post_single = is_single() ? 'tps-post-single' : 'tps-posts-grid';

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $post_single ); ?>>
	<?php
	if ( is_single() ) :
		get_template_part( 'template-parts/content', 'blog-single' );
	else :
		get_template_part( 'template-parts/content', 'blog-grid' );
	endif;
	?>
</article><!-- #post-## -->
