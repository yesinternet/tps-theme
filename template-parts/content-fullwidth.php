<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tps
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	$image_url     = get_the_post_thumbnail_url( '', 'tps_image_xlarge' );
	$post_id       = get_the_ID();
	$promo_heading = get_post_meta( $post_id, '_tps_promo_text', true );
	if ( $image_url ) :?>
        <div class="tps-image-full-height">
            <img src="<?php echo $image_url; ?>" class="img-responsive"/>
            <div class="tps-image-text-container">
                <div class="tps-image-text">
					<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
					<?php if ( ! empty( $promo_heading ) ): ?>
                        <h2><?php echo $promo_heading; ?></h2>
					<?php endif; ?>
                </div>
            </div>
            <div class="tps-image-full-height-arrow tps-bounce"><a href="#" class="tps-scrollDown"><i class="fa fa-chevron-down fa-3x"></i></a></div>
        </div>
	<?php endif; ?>

	<?php //get_template_part( 'global/tps-breadcrumbs' ); ?>

	<?php if ( ! $image_url ) : ?>
        <header class="entry-header">
			<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
	<?php endif; ?>

    <div class="page-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tps' ),
			'after'  => '</div>',
		) );
		?>
    </div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
        <footer class="page-footer">
			<?php
			edit_post_link(
				sprintf(
				/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'tps' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
        </footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->