var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({ camelize: true });
var lr = require('tiny-lr');
var server = lr();
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var merge = require('merge-stream');
var connect = require('gulp-connect');


gulp.task('styles', function () {
    gulp.src('./assets/scss/**/*.scss')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        // .pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./assets/build'))
        .pipe(plugins.cleanCss({ keepSpecialComments: 1 }))
        // .pipe(plugins.livereload(server))
        .pipe(gulp.dest('./css'))
        .pipe(connect.reload())
        .pipe(plugins.notify({ message: 'Styles task complete' }));
});

// Site Scripts
/*gulp.task('scripts', function() {
    return gulp.src(['assets/js/source/!*.js', '!assets/js/source/plugins.js'])
        .pipe(plugins.jshint('.jshintrc'))
        .pipe(plugins.jshint.reporter('default'))
        .pipe(plugins.concat('main.js'))
        .pipe(gulp.dest('assets/js/build'))
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.uglify())
        .pipe(plugins.livereload(server))
        .pipe(gulp.dest('assets/js'))
        .pipe(plugins.notify({ message: 'Scripts task complete' }));
});*/
/*gulp.task('scripts', function() {
    return gulp.src([
        'assets/js/!*.js',
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js'])
        // .pipe(plugins.jshint('.jshintrc'))
        // .pipe(plugins.jshint.reporter('default'))
        .pipe(plugins.concat('scripts.js'))
        .pipe(gulp.dest('assets/js/build'))
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.uglify())
        // .pipe(plugins.livereload(server))
        .pipe(gulp.dest('./js'))
        .pipe(plugins.notify({ message: 'Scripts task complete' }));
});*/

// Images
/*gulp.task('images', function() {
    return gulp.src('assets/images/!**!/!*')
        .pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })))
        .pipe(plugins.livereload(server))
        .pipe(gulp.dest('assets/images'))
        .pipe(plugins.notify({ message: 'Images task complete' }));
});*/

// Watch
gulp.task('watch', function() {

    // Listen on port 35729
    // server.listen(35729, function (err) {
    //     if (err) {
    //         return console.log(err)
    //     };

        // Watch .scss files
        gulp.watch('./assets/scss/**/*.scss', ['styles']);

        // Watch .js files
        // gulp.watch('assets/js/**/*.js', ['plugins', 'scripts']);

        // Watch image files
        // gulp.watch('assets/images/**/*', ['images']);

    // });

});

gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});

// Default task
// gulp.task('default', ['styles', 'plugins', 'scripts', 'images', 'watch']);
gulp.task('default', ['styles', 'watch', 'connect']);