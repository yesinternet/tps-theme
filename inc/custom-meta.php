<?php
$site_url = site_url();
?>


<link rel="icon" type="image/png" href="<?php echo $site_url; ?>/files/tps-assets/favicon/favicon.ico"/>
<link rel="icon" type="image/png" href="<?php echo $site_url; ?>/files/tps-assets/favicon/favicon-196x196.png" sizes="196x196"/>
<link rel="icon" type="image/png" href="<?php echo $site_url; ?>/files/tps-assets/favicon/favicon-96x96.png" sizes="96x96"/>
<link rel="icon" type="image/png" href="<?php echo $site_url; ?>/files/tps-assets/favicon/favicon-32x32.png" sizes="32x32"/>
<link rel="icon" type="image/png" href="<?php echo $site_url; ?>/files/tps-assets/favicon/favicon-16x16.png" sizes="16x16"/>
<link rel="icon" type="image/png" href="<?php echo $site_url; ?>/files/tps-assets/favicon/favicon-128.png" sizes="128x128"/>

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $site_url; ?>/files/tps-assets/apple/apple-touch-icon-57x57.png"/>
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $site_url; ?>/files/tps-assets/apple/apple-touch-icon-72x72.png"/>
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $site_url; ?>/files/tps-assets/apple/apple-touch-icon-144x144.png"/>
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $site_url; ?>/files/tps-assets/apple/apple-touch-icon-60x60.png"/>
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $site_url; ?>/files/tps-assets/apple/apple-touch-icon-120x120.png"/>
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $site_url; ?>/files/tps-assets/apple/apple-touch-icon-76x76.png"/>
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $site_url; ?>/files/tps-assets/apple/apple-touch-icon-152x152.png"/>

<meta name="application-name" content="The Parents Shop"/>
<meta name="msapplication-navbutton-color" content="#00c7b3">
<meta name="msapplication-square150x150logo" content="<?php echo $site_url; ?>/files/tps-assets/msapplication/logo-150x150.png">
<meta name="msapplication-square310x310logo" content="<?php echo $site_url; ?>/files/tps-assets/msapplication/logo-310x310.png">
<meta name="msapplication-square70x70logo" content="<?php echo $site_url; ?>/files/tps-assets/msapplication/logo-70x70.png">
<meta name="msapplication-wide310x150logo" content="<?php echo $site_url; ?>/files/tps-assets/msapplication/logo-310x150.png">
<meta name="msapplication-starturl" content="<?php echo $site_url; ?>">
<meta name="msapplication-TileColor" content="#00c7b3">
<meta name="msapplication-TileImage" content="<?php echo $site_url; ?>/files/tps-assets/msapplication/logo-144x144.png">
<meta name="msapplication-tooltip" content="The Parents Shop app">
<meta name="msapplication-config" content="<?php echo $site_url; ?>/files/tps-assets/msapplication/ieconfig.xml">

<?php //affiliate verification ?>
<meta name="verification" content="8962f27f22831641482eb5a94d2b21d0"/>
