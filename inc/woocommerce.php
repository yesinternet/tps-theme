<?php
// No direct access, please
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Add WooCommerce support
 */

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

/**
 * Remove default WooCommerce wrappers
 */

remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

add_filter( 'woocommerce_enqueue_styles', 'tps_dequeue_styles' );
function tps_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );    // Remove the gloss
	unset( $enqueue_styles['woocommerce-layout'] );        // Remove the layout
	unset( $enqueue_styles['woocommerce-smallscreen'] );    // Remove the smallscreen optimisation

	return $enqueue_styles;
}

/**
 * Remove WooCommerce generator
 */

function tps_remove_woocommerce_generator_tag() {
	remove_action( 'get_the_generator_html', 'wc_generator_tag', 10 );
	remove_action( 'get_the_generator_xhtml', 'wc_generator_tag', 10 );
}

add_action( 'get_header', 'tps_remove_woocommerce_generator_tag' );

function tps_woocommerce_script_cleaner() {
//    if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
//        wp_dequeue_script( 'wc-add-payment-method' );
//        wp_dequeue_script( 'wc-lost-password' );
//        wp_dequeue_script( 'wc_price_slider' );
//        wp_dequeue_script( 'wc-single-product' );
	wp_dequeue_script( 'wc-add-to-cart' );
	wp_dequeue_script( 'wc-cart-fragments' );
//        wp_dequeue_script( 'wc-credit-card-form' );
//        wp_dequeue_script( 'wc-checkout' );
//        wp_dequeue_script( 'wc-add-to-cart-variation' );
//        wp_dequeue_script( 'wc-single-product' );
//        wp_dequeue_script( 'wc-cart' );
//        wp_dequeue_script( 'wc-chosen' );
//        wp_dequeue_script( 'woocommerce' );
//        wp_dequeue_script( 'prettyPhoto' );
//        wp_dequeue_script( 'prettyPhoto-init' );
//        wp_dequeue_script( 'jquery-blockui' );
//        wp_dequeue_script( 'jquery-placeholder' );
//        wp_dequeue_script( 'jquery-payment' );
//        wp_dequeue_script( 'fancybox' );
//        wp_dequeue_script( 'jqueryui' );
//    }
}

add_action( 'wp_enqueue_scripts', 'tps_woocommerce_script_cleaner', 99 );

require get_template_directory() . '/woocommerce-hooks/tps-hooks.php';