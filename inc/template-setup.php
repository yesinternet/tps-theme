<?php

if ( ! function_exists( 'tps_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tps_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on tps, use a find and replace
		 * to change 'tps' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'tps', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'tps' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'tps_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'post-formats', array(
			'aside',
			'gallery',
			'quote',
			'image',
			'video'
		) );

		add_theme_support( 'custom-logo', array(
			'width'       => 400,
			'height'      => 75,
			'flex-width'  => true,
			'flex-height' => true,
			'header-text' => '',
		) );

		add_image_size( 'tps_image_xlarge', 1920, 1080, true );
		add_image_size( 'tps_image_large', 1920, 800, true );
		add_image_size( 'tps_image_medium', 1366, 768, true );
		add_image_size( 'tps_image_blog_grid', 400, 225, true );

		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

endif;

add_action( 'after_setup_theme', 'tps_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tps_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tps_content_width', 640 );
}

add_action( 'after_setup_theme', 'tps_content_width', 0 );

/**
 * Add bootstrap image class to images
 */
function tps_set_attachment_image_attributes( $attr, $attachment, $size ) {
	$attr['class'] .= ' img-responsive';

	return $attr;
}

add_filter( 'wp_get_attachment_image_attributes', 'tps_set_attachment_image_attributes', 10, 3 );

/**
 * Add page slug class to body
 */
function tps_page_slug_class( $classes, $class ) {
	global $wp_query;
	if ( is_page() ) {
		$page_id = $wp_query->get_queried_object_id();

		$post = get_post( $page_id );

		if ( $post->post_name ) {
			$classes[] = 'page-' . sanitize_html_class( $post->post_name );
		}
	}

	return $classes;
}

add_filter( 'body_class', 'tps_page_slug_class', 10, 2 );

/**
 * Customize login page
 */

function remove_login_logo() {
	?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: none;
            width: 100%;
            text-indent: 0px;
            outline: 0;
            display: block;
        }
    </style>
	<?php
}

add_action( 'login_enqueue_scripts', 'remove_login_logo' );

function my_login_logo_url() {
	return home_url();
}

add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
	return get_bloginfo( 'name' );
}

add_filter( 'login_headertitle', 'my_login_logo_url_title' );


/**
 * Contact Form 7 disable scripts in all pages
 */

add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );


/**
 * Responsive video plugin styles load only when video exists
 */

function tps_remove_responsive_video_styles() {
	remove_action( 'wp_enqueue_scripts', 'wc_responsive_video_scripts' );
}

add_action( 'wp_loaded', 'tps_remove_responsive_video_styles' );

function tps_add_responsive_video_styles( $html, $url, $attr, $post_ID ) {
	$show_styles = true;
	if ( ! preg_match( '/^<iframe.*\<\/iframe\>$/', $html ) ) {
		$show_styles = false;
	}
	if ( function_exists( 'wc_responsive_video_html_get_ratio' ) && ! $ratio = wc_responsive_video_html_get_ratio( $html ) ) {
		$show_styles = false;
	}

	if ( $show_styles && function_exists( 'wc_responsive_video_scripts' ) ) {
		wc_responsive_video_scripts();
	}

	return $html;

}

add_filter( 'embed_oembed_html', 'tps_add_responsive_video_styles', 998, 4 );

function tps_remove_youtube_video_title( $html ) {
	if ( ! is_home() && ! is_archive() ) {
		return $html;
	}

	if ( strpos( $html, 'youtu.be' ) !== false || strpos( $html, 'youtube.com' ) !== false ) {
		$return = preg_replace( "@src=(['\"])?([^'\">\s]*)@", "src=$1$2&showinfo=0&rel=0&autohide=1", $html );

		return $return;
	}

	return $html;
}

add_filter( 'embed_handler_html', 'tps_remove_youtube_video_title' );
add_filter( 'embed_oembed_html', 'tps_remove_youtube_video_title' );

function tps_social_login_redirect( $query ) {

	if ( ! defined( 'TPS_PROFILE_PAGE' ) || empty( $query->request ) ) {
		return;
	}

	if ( $query->request == TPS_PROFILE_PAGE ) {

		$referer = wp_get_referer();

		if ( ! empty( $_REQUEST['redirect_to'] ) ) {
			if ( ! empty( $referer ) && $referer == $_REQUEST['redirect_to'] ) {
				return;
			}
			wp_safe_redirect( $_REQUEST['redirect_to'] );
			exit();
		}

		if ( ! empty( $referer ) ) {
			$_REQUEST['redirect_to'] = $referer;
			$_SERVER["REQUEST_URI"]  = add_query_arg( array( "redirect_to" => $referer ) );
		}
	}
}

add_action( 'parse_request', 'tps_social_login_redirect' );

function tps_add_profile_menu( $items ) {

	ob_start();

	get_template_part( 'global/tps-menu-profile' );

	$profile_menu = ob_get_clean();

	$items .= $profile_menu;

	return $items;
}

add_filter( 'wp_nav_menu_items', 'tps_add_profile_menu' );


function tps_comment_form_submit_button( $submit_button, $args ) {

	$args['class_submit'] .= ' btn btn-primary';

	$submit_button = sprintf(
		$args['submit_button'],
		esc_attr( $args['name_submit'] ),
		esc_attr( $args['id_submit'] ),
		esc_attr( $args['class_submit'] ),
		esc_attr( $args['label_submit'] )
	);

	return $submit_button;
}

add_filter( 'comment_form_submit_button', 'tps_comment_form_submit_button', 10, 2 );

function tps_logout_redirect( $redirect_to, $requested_redirect_to, $user ) {

	$current_url = wp_get_referer();

	if ( ! empty( $current_url ) ) {
		return $current_url;
	}

	return $redirect_to;
}

add_filter( 'logout_redirect', 'tps_logout_redirect', 10, 3 );


//Facebook login and iThemes Security fix
//Remove filter from iThemes Security
/*function tps_remove_filter_with_function_from_class($tag, $function_to_remove, $priority = 10 ){
    global $wp_filter;
    if(!is_admin()){
        if(isset($wp_filter[$tag]->callbacks[$priority])){
            $functions = $wp_filter[$tag]->callbacks[$priority];
            foreach ($functions as $key => $value) {
                if(strpos($key, $function_to_remove) !== false){
                    remove_filter($tag, $key, $priority);
                }
            }
        }
    }
}

tps_remove_filter_with_function_from_class('site_url', 'filter_generated_url', 100);*/

//OR remove itsec-hb-token from url with filter in wp_login_url function
function tps_remove_itsec( $login_url ) {
	$login_url = remove_query_arg( 'itsec-hb-token', $login_url );

	return $login_url;
}

add_filter( 'login_url', 'tps_remove_itsec' );

