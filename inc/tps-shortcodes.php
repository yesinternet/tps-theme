<?php

class TPS_Shortcodes {

	/**
	 * Init shortcodes.
	 */
	public static function init() {
		if ( ! function_exists( 'WC' ) ) {
			return;
		}
		$shortcodes = array(
			'tps_product_category'                   => __CLASS__ . '::product_category',
			'tps_product_categories'                 => __CLASS__ . '::product_categories',
			'tps_featured_products'                  => __CLASS__ . '::featured_products',
			'tps_sale_products'                      => __CLASS__ . '::sale_products',
			'tps_recent_products'                    => __CLASS__ . '::recent_products',
			'tps_products'                           => __CLASS__ . '::products',
			'tps_product_cat_products_slider'        => __CLASS__ . '::product_cat_products_slider',
			'tps_homepage_products_primary_slider'   => __CLASS__ . '::homepage_products_primary_slider',
			'tps_homepage_products_secondary_slider' => __CLASS__ . '::homepage_products_secondary_slider',
			'tps_homepage_product_cat_slider'        => __CLASS__ . '::homepage_product_cat_slider',
			'tps_slider'                             => __CLASS__ . '::slider',
			'tps_row'                                => __CLASS__ . '::row',
			'tps_column'                             => __CLASS__ . '::column',
			'tps_sell'                               => __CLASS__ . '::sell',
			'tps_about'                              => __CLASS__ . '::about',
			'tps_press'                              => __CLASS__ . '::press',
			'tps_contact'                            => __CLASS__ . '::contact',
			'tps_social_icons'                       => __CLASS__ . '::post_social_icons',
			'tps_posts_categories'                   => __CLASS__ . '::posts_categories',
			'tps_faq'                                => __CLASS__ . '::faq'
		);

		foreach ( $shortcodes as $shortcode => $function ) {
			add_shortcode( $shortcode, $function );
		}

	}

	/**
	 * Loop over found products.
	 *
	 * @param array $query_args
	 * @param array $atts
	 * @param string $loop_name
	 *
	 * @return string
	 */
	private static function product_loop( $query_args, $atts, $loop_name ) {
		global $woocommerce_loop;
		global $tps_shown_products;
		global $tps_shortcode_name;
		global $products;

		$columns                     = absint( $atts['columns'] );
		$woocommerce_loop['columns'] = $columns;
		$woocommerce_loop['name']    = $loop_name;
		$query_args['paged']         = isset( $query_args['paged'] ) ? $query_args['paged'] : get_query_var( 'paged' );
		$query_args                  = apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, $loop_name );
		$transient_name              = 'wc_loop' . substr( md5( json_encode( $query_args ) . $loop_name ), 28 ) . WC_Cache_Helper::get_transient_version( 'product_query' );
		$products                    = get_transient( $transient_name );
		$template_name               = 'product' . ( ! empty( $atts['slider'] ) ? '_slider' : '' );
		$tps_shortcode_name          = $loop_name;

		if ( false === $products || ! is_a( $products, 'WP_Query' ) ) {
			$products = new WP_Query( $query_args );
			set_transient( $transient_name, $products, DAY_IN_SECONDS * 30 );
		}

		ob_start();

		if ( $products->have_posts() ) {

			// Prime caches before grabbing objects.
			update_post_caches( $products->posts, array( 'product', 'product_variation' ) );
			?>

			<?php do_action( "tps_shortcode_before_{$loop_name}_loop" ); ?>

			<?php woocommerce_product_loop_start( false ); ?>

			<?php while ( $products->have_posts() ) : $products->the_post(); ?>

				<?php get_template_part( 'shortcode-templates/content', $template_name ); ?>

			<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end( false ); ?>

			<?php if ( $atts['pagination'] == 'true' ) {
				get_template_part( 'shortcode-templates/shortcode-pagination' );
			} ?>

			<?php do_action( "tps_shortcode_after_{$loop_name}_loop" ); ?>

			<?php
		} else {
			do_action( "tps_shortcode_{$loop_name}_loop_no_results" );
		}

		woocommerce_reset_loop();
		wp_reset_postdata();

//        return '<div class="tps-shortcode row">' . ob_get_clean() . '</div>';
		return ob_get_clean();
	}

	/**
	 * Output featured products.
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	public static function featured_products( $atts ) {
		$atts['per_page'] = ! empty( $atts['max_slides'] ) ? $atts['max_slides'] : ( ( ! empty( $atts['per_page'] ) ) ? $atts['per_page'] : '12' );
		$atts             = shortcode_atts( array(
			'per_page'         => '12',
			'columns'          => '4',
			'orderby'          => 'date',
			'order'            => 'desc',
			'category'         => '',  // Slugs
			'operator'         => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
			'exclude_products' => '', //Ids separated with ','
			'slider'           => false,
			'fullwidth'        => 'false',
			'pagination'       => 'false',
		), $atts, 'featured_products' );

		$meta_query  = WC()->query->get_meta_query();
		$tax_query   = WC()->query->get_tax_query();
		$tax_query[] = array(
			'taxonomy' => 'product_visibility',
			'field'    => 'name',
			'terms'    => 'featured',
			'operator' => 'IN',
		);

		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $atts['per_page'],
			'orderby'             => $atts['orderby'],
			'order'               => $atts['order'],
			'meta_query'          => $meta_query,
			'tax_query'           => $tax_query,
		);

		if ( ! empty( $atts['exclude_products'] ) ) {
			$atts['exclude_products']   = explode( ',', $atts['exclude_products'] );
			$query_args['post__not_in'] = $atts['exclude_products'];
		}

		$query_args = self::_maybe_add_category_args( $query_args, $atts['category'], $atts['operator'] );

		$return = self::product_loop( $query_args, $atts, 'featured_products' );

		$header = '<h3 class="tps-shortcode-grid-header text-center">' . __( 'Recommended', 'tps' ) . '</h3>';
		$return = self::_get_shortcode_wrapper( $return, $atts, $header );

		return $return;
	}

	/**
	 * List products in a category shortcode.
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	public static function product_category( $atts ) {
		$atts['per_page'] = ! empty( $atts['max_slides'] ) ? $atts['max_slides'] : ( ( ! empty( $atts['per_page'] ) ) ? $atts['per_page'] : '12' );
		$atts             = shortcode_atts( array(
			'per_page'                    => '12',
			'columns'                     => '4',
			'orderby'                     => 'menu_order title',
			'order'                       => 'asc',
			'category'                    => '',  // Slugs
			'operator'                    => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
			'product_cat_slider_products' => false,
			'slider'                      => false,
			'fullwidth'                   => 'false',
			'pagination'                  => 'false',
		), $atts, 'product_category' );

		if ( ! $atts['category'] ) {
			return '';
		}

		// Default ordering args
		$ordering_args = WC()->query->get_catalog_ordering_args( $atts['orderby'], $atts['order'] );
		$meta_query    = WC()->query->get_meta_query();
		if ( $atts['product_cat_slider_products'] == 'true' ) {
			$meta_query[] = array(
				'key'   => '_tps_product_cat_slider',
				'value' => 'yes'
			);
		}

		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'orderby'             => $ordering_args['orderby'],
			'order'               => $ordering_args['order'],
			'posts_per_page'      => $atts['per_page'],
			'meta_query'          => $meta_query,
			'tax_query'           => WC()->query->get_tax_query(),
		);

		$query_args = self::_maybe_add_category_args( $query_args, $atts['category'], $atts['operator'] );

		if ( isset( $ordering_args['meta_key'] ) ) {
			$query_args['meta_key'] = $ordering_args['meta_key'];
		}

		$return = self::product_loop( $query_args, $atts, 'product_cat' );

		$return = self::_get_shortcode_wrapper( $return, $atts );

		// Remove ordering query arguments
		WC()->query->remove_ordering_args();

		return $return;
	}

	/**
	 * List all (or limited) product categories.
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	public static function product_categories( $atts ) {
		global $woocommerce_loop;

		$atts          = shortcode_atts( array(
			'number'                      => null,
			'orderby'                     => 'name',
			'order'                       => 'ASC',
			'columns'                     => '4',
			'hide_empty'                  => 1,
			'parent'                      => '',
			'ids'                         => '',
			'homepage_product_cat_slider' => false,
			'slider'                      => false,
			'fullwidth'                   => 'false',
		), $atts, 'product_categories' );
		$template_name = 'content-product_cat' . ( ! empty( $atts['slider'] ) ? '_slider' : '' ) . '.php';

		$ids        = array_filter( array_map( 'trim', explode( ',', $atts['ids'] ) ) );
		$hide_empty = ( $atts['hide_empty'] == true || $atts['hide_empty'] == 1 ) ? 1 : 0;

		// get terms and workaround WP bug with parents/pad counts
		$args = array(
			'orderby'    => $atts['orderby'],
			'order'      => $atts['order'],
			'hide_empty' => $hide_empty,
			'include'    => $ids,
			'pad_counts' => true,
			'child_of'   => $atts['parent']
		);

		if ( $atts['homepage_product_cat_slider'] == 'true' ) {
			$args['meta_query'][] = array(
				'key'   => '_tps_homepage_product_cat_slider',
				'value' => 'yes'
			);
		}

		$product_categories = get_terms( 'product_cat', $args );

		if ( '' !== $atts['parent'] ) {
			$product_categories = wp_list_filter( $product_categories, array( 'parent' => $atts['parent'] ) );
		}

		if ( $hide_empty ) {
			foreach ( $product_categories as $key => $category ) {
				if ( $category->count == 0 ) {
					unset( $product_categories[ $key ] );
				}
			}
		}

		if ( $atts['number'] ) {
			$product_categories = array_slice( $product_categories, 0, $atts['number'] );
		}

		$columns                     = absint( $atts['columns'] );
		$woocommerce_loop['columns'] = $columns;

		ob_start();

		if ( $product_categories ) {
			woocommerce_product_loop_start( false );

			foreach ( $product_categories as $category ) {
				wc_get_template( $template_name, array(
					'category' => $category
				) );
			}

			woocommerce_product_loop_end( false );
		}

		woocommerce_reset_loop();

		$return = ob_get_clean();

		$return = trim( $return );

		if ( $atts['slider'] == false && ! empty( $return ) ) {
			$return = '<div class="categories row">' . $return . '</div>';
			if ( $atts['fullwidth'] == 'true' ) {
				$return = '<div class="tps-shortcode container-fluid">' . $return . '</div>';
			} else {
				$return = '<div class="tps-shortcode container">' . $return . '</div>';
			}
		}

		return $return;
	}

	/**
	 * List multiple products shortcode.
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	public static function products( $atts ) {
		$atts['per_page'] = ! empty( $atts['max_slides'] ) ? $atts['max_slides'] : ( ( ! empty( $atts['per_page'] ) ) ? $atts['per_page'] : - 1 );
		$atts             = shortcode_atts( array(
			'columns'                            => '4',
			'orderby'                            => 'title',
			'order'                              => 'asc',
			'ids'                                => '',
			'skus'                               => '',
			'per_page'                           => - 1,
			'homepage_products_primary_slider'   => false,
			'homepage_products_secondary_slider' => false,
			'slider'                             => false,
			'fullwidth'                          => 'false',
			'pagination'                         => 'false',
		), $atts, 'products' );

		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'orderby'             => $atts['orderby'],
			'order'               => $atts['order'],
			'posts_per_page'      => $atts['per_page'],
			'meta_query'          => WC()->query->get_meta_query(),
			'tax_query'           => WC()->query->get_tax_query(),
		);

		if ( ! empty( $atts['skus'] ) ) {
			$query_args['meta_query'][] = array(
				'key'     => '_sku',
				'value'   => array_map( 'trim', explode( ',', $atts['skus'] ) ),
				'compare' => 'IN'
			);

			// Ignore catalog visibility
			$query_args['meta_query'] = array_merge( $query_args['meta_query'], WC()->query->stock_status_meta_query() );
		}

		if ( ! empty( $atts['ids'] ) ) {
			$query_args['post__in'] = array_map( 'trim', explode( ',', $atts['ids'] ) );

			// Ignore catalog visibility
			$query_args['meta_query'] = array_merge( $query_args['meta_query'], WC()->query->stock_status_meta_query() );
		}

		if ( $atts['homepage_products_primary_slider'] == 'true' ) {
			$query_args['meta_query'][] = array(
				'key'   => '_tps_homepage_primary_slider',
				'value' => 'yes'
			);
			$query_args['meta_query']   = array_merge( $query_args['meta_query'], WC()->query->stock_status_meta_query() );
		}

		if ( $atts['homepage_products_secondary_slider'] == 'true' ) {
			$query_args['meta_query'][] = array(
				'key'   => '_tps_homepage_secondary_slider',
				'value' => 'yes'
			);
			$query_args['meta_query']   = array_merge( $query_args['meta_query'], WC()->query->stock_status_meta_query() );
		}

		$return = self::product_loop( $query_args, $atts, 'products' );

		$return = self::_get_shortcode_wrapper( $return, $atts );

		return $return;
	}

	/**
	 * List all products on sale.
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	public static function sale_products( $atts ) {
		$atts = shortcode_atts( array(
			'per_page'   => '12',
			'columns'    => '4',
			'orderby'    => 'title',
			'order'      => 'asc',
			'category'   => '', // Slugs
			'operator'   => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
			'slider'     => false,
			'fullwidth'  => 'false',
			'pagination' => 'false',
		), $atts, 'sale_products' );

		$query_args = array(
			'posts_per_page' => $atts['per_page'],
			'orderby'        => $atts['orderby'],
			'order'          => $atts['order'],
//            'no_found_rows'  => 1,
			'post_status'    => 'publish',
			'post_type'      => 'product',
			'meta_query'     => WC()->query->get_meta_query(),
			'tax_query'      => WC()->query->get_tax_query(),
			'post__in'       => array_merge( array( 0 ), wc_get_product_ids_on_sale() )
		);

		$query_args = self::_maybe_add_category_args( $query_args, $atts['category'], $atts['operator'] );

		$return = self::product_loop( $query_args, $atts, 'sale_products' );

		$return = self::_get_shortcode_wrapper( $return, $atts );

		return $return;
	}

	/**
	 * Recent Products shortcode.
	 *
	 * @param array $atts
	 *
	 * @return string
	 */
	public static function recent_products( $atts ) {
		$atts = shortcode_atts( array(
			'per_page'   => '12',
			'columns'    => '4',
			'orderby'    => 'date',
			'order'      => 'desc',
			'category'   => '',  // Slugs
			'operator'   => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
			'slider'     => false,
			'fullwidth'  => 'false',
			'pagination' => 'false',
		), $atts, 'recent_products' );

		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $atts['per_page'],
			'orderby'             => $atts['orderby'],
			'order'               => $atts['order'],
			'meta_query'          => WC()->query->get_meta_query(),
			'tax_query'           => WC()->query->get_tax_query(),
		);

		$query_args = self::_maybe_add_category_args( $query_args, $atts['category'], $atts['operator'] );

		$return = self::product_loop( $query_args, $atts, 'recent_products' );

		$return = self::_get_shortcode_wrapper( $return, $atts );

		return $return;
	}

	public static function product_cat_products_slider( $atts ) {
		$atts['product_cat_slider_products'] = 'true';
		$atts['type']                        = 'product_category';
		$return                              = self::slider( $atts );

		return $return;
	}

	public static function homepage_products_primary_slider( $atts ) {
		$atts['homepage_products_primary_slider'] = 'true';
		$atts['type']                             = 'products';
		$return                                   = self::slider( $atts );

		return $return;
	}

	public static function homepage_products_secondary_slider( $atts ) {
		$atts['homepage_products_secondary_slider'] = 'true';
		$atts['type']                               = 'products';
		$return                                     = self::slider( $atts );

		return $return;
	}

	public static function homepage_product_cat_slider( $atts ) {
		$atts['homepage_product_cat_slider'] = 'true';
		$atts['type']                        = 'product_categories';
		$return                              = self::slider( $atts );

		return $return;
	}

	public static function slider( $atts ) {
		$navText        = array(
			"'<span class=\"fa fa-fw fa-chevron-left\"></span>'",
			"'<span class=\"fa fa-fw fa-chevron-right\"></span>'"
		);
		$atts['slider'] = true;
		$attribs        = shortcode_atts( array(
			'id'                 => '0',
			'type'               => '',
			'items'              => '1',
			'loop'               => 'true',
			'center'             => 'false',
			'stagePadding'       => 0,
			'nav'                => 'true',
			'navText'            => $navText,
			'autoplay'           => 'true',
			'lazyload'           => 'false',
			'fullwidth'          => 'false',
			'fullheight'         => 'false',
			'max_slides'         => '4',
			'slider_button_text' => ''
		), $atts, 'slider' );

		if ( empty( $attribs['type'] ) ) {
			return '';
		}

		if ( ! method_exists( __CLASS__, $attribs['type'] ) ) {
			return '';
		}

		global $tps_slider;
		global $tps_index;
		$tps_index = 0;

		$tps_slider = $attribs;

		$content = call_user_func( array( __CLASS__, $attribs['type'] ), $atts );

		ob_start();

		$tps_slider['slides'] = $content;

		get_template_part( 'global/slider-wrapper' );

		$return = ob_get_clean();

		$tps_slider = array();

		return $return;
	}

	public static function row( $atts, $content = '' ) {

		if ( empty( $content ) ) {
			return $content;
		}


		$content = '<div class="row">' . do_shortcode( $content ) . '</div>';

		return $content;
	}

	public static function column( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'size'   => '12',
			'screen' => 'xs'
		), $atts, 'column' );

		if ( empty( $content ) ) {
			return $content;
		}

		$size   = $atts['size'];
		$screen = $atts['screen'];

		$content = '<div class="col-' . $screen . '-' . $size . '">' . do_shortcode( $content ) . '</div>';

		return $content;
	}

	public static function sell( $atts ) {
		global $tps_shortcode_sell_atts;
		$atts = shortcode_atts( array(
			'image_ids' => ''
		), $atts, 'sell' );

		$atts['image_ids'] = ! empty( $atts['image_ids'] ) ? explode( ',', $atts['image_ids'] ) : array();

		$tps_shortcode_sell_atts = $atts;

		ob_start();

		get_template_part( 'shortcode-templates/sell-page' );

		$return = ob_get_clean();

		return $return;
	}

	public static function about( $atts ) {
		global $tps_shortcode_about_atts;
		$atts = shortcode_atts( array(
			'image_ids' => ''
		), $atts, 'about' );

		$atts['image_ids'] = ! empty( $atts['image_ids'] ) ? explode( ',', $atts['image_ids'] ) : array();

		$tps_shortcode_about_atts = $atts;

		ob_start();

		get_template_part( 'shortcode-templates/about-page' );

		$return = ob_get_clean();

		return $return;
	}

	public static function contact( $atts ) {
		global $tps_shortcode_contact_atts;
		$atts = shortcode_atts( array(
			'image_ids' => '',
			'id'        => '',
			'title'     => 'Contact Form'

		), $atts, 'contact' );

		$atts['image_ids'] = ! empty( $atts['image_ids'] ) ? explode( ',', $atts['image_ids'] ) : array();

		$tps_shortcode_contact_atts = $atts;

		ob_start();

		get_template_part( 'shortcode-templates/contact-page' );

		$return = ob_get_clean();

		return $return;
	}

	public static function press( $atts ) {
		global $tps_shortcode_press_atts;
		$atts = shortcode_atts( array(
			'image_ids' => ''

		), $atts, 'press' );

		$atts['image_ids'] = ! empty( $atts['image_ids'] ) ? explode( ',', $atts['image_ids'] ) : array();

		$tps_shortcode_press_atts = $atts;

		ob_start();

		get_template_part( 'shortcode-templates/press-page' );

		$return = ob_get_clean();

		return $return;
	}

	public static function post_social_icons( $atts ) {
		$atts = shortcode_atts( array(
			'is_button' => 0

		), $atts, 'post_social_icons' );

		global $tps_post_social_url;
		global $tps_post_pinterest_url;
		global $tps_post_social_title;

		$tps_post_id = get_the_ID();

		if ( empty( $tps_post_id ) ) {
			return '';
		}

		$tps_post_url = urlencode( get_the_permalink( $tps_post_id ) );

		$tps_post_image_url = urlencode( get_the_post_thumbnail_url( '', 'tps_image_blog_grid' ) );

		$tps_post_title = get_the_title( $tps_post_id );

		$tps_post_pinterest_description = urlencode( wp_strip_all_tags( $tps_post_title, true ) );
		$tps_post_pinterest_url         = "//pinterest.com/pin/create/button/?url=" . $tps_post_url . "&media=" . $tps_post_image_url . "&description=" . $tps_post_pinterest_description;

		$tps_post_social_url = $tps_post_url;

		$tps_post_social_title = $tps_post_title;


		echo '<div class="tps-post-icons">';
		if ( $atts['is_button'] != 0 ) {
			get_template_part( "global/social-share-button" );
		} else {
			get_template_part( "global/social-share-icons" );
		}
		echo '</div>';
	}

	public static function posts_categories( $atts ) {
		$atts = shortcode_atts( array(
			'secondary' => 0
		), $atts, 'posts_categories' );

//        $cat_id = 0;
//        $category = get_category(get_query_var('cat'));
//
//        if($atts['secondary'] == 0) {
//            $cat_id = !empty($category->parent) ? $category->parent : 0;
//        }else{
//            $cat_id = !empty($category->cat_ID) ? $category->cat_ID : 0;
//        }

		$categories = wp_list_categories( array(
			'title_li'         => '',
			'echo'             => 0,
			'hide_empty'       => 1,
			'depth'            => 0,
			'child_of'         => 0,
			'show_option_none' => false,
			'show_option_all'  => 'All'
			//'walker' => new Tps_Category_Walker()*/
		) );

		if ( empty( $categories ) ) {
			return '';
		}

		echo '<ul class="tps-posts-categories ' . ( $atts['secondary'] == 0 ? 'primary' : 'secondary' ) . '">';
		echo $categories;
		echo '</ul>';
	}

	public static function faq( $atts ) {
		global $tps_faq_posts;

		$atts = shortcode_atts( array(
			'parent_id' => 0
		), $atts, 'faq' );

		$parent_id = $atts['parent_id'];

		if ( empty( $parent_id ) ) {
			return '';
		}

		$args = array(
			'numberposts' => - 1,
			'post_type'   => 'page',
			'post_status' => 'publish',
			'post_parent' => $parent_id,
			'orderby'     => array(
				'menu_order' => 'ASC',
				'date'       => 'DESC'
			)
		);

		$tps_faq_posts = get_children( $args );

		ob_start();

		get_template_part( 'shortcode-templates/faq-accordion' );

		$return = ob_get_clean();

		return $return;

	}

	/**
	 * Adds a tax_query index to the query to filter by category.
	 *
	 * @param array $args
	 * @param string $category
	 * @param string $operator
	 *
	 * @return array;
	 * @access private
	 */
	private static function _maybe_add_category_args( $args, $category, $operator ) {
		if ( ! empty( $category ) ) {
			if ( empty( $args['tax_query'] ) ) {
				$args['tax_query'] = array();
			}
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'product_cat',
					'terms'    => array_map( 'sanitize_title', explode( ',', $category ) ),
					'field'    => 'slug',
					'operator' => $operator
				)
			);
		}

		return $args;
	}

	private static function _get_shortcode_wrapper( $return, $atts, $header = '' ) {
		$return = trim( $return );
		if ( $atts['slider'] == false && ! empty( $return ) ) {
			$return = $header . '<div class="products row">' . $return . '</div>';
			if ( $atts['fullwidth'] == 'true' ) {
				$return = '<div class="tps-shortcode container-fluid">' . $return . '</div>';
			} else {
				$return = '<div class="tps-shortcode container">' . $return . '</div>';
			}
		}

		return $return;
	}

	private static function get_featured_meta_query( $meta_query, $featured ) {
		if ( ! is_array( $meta_query ) ) {
			$meta_query = array();
		}
		if ( $featured ) {
			$meta_query['featured'] = array(
				'key'   => '_featured',
				'value' => 'yes'
			);
		}

		return $meta_query;
	}


}

add_action( 'init', array( 'TPS_Shortcodes', 'init' ) );