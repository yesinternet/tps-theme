<?php

/**
 * Enqueue scripts and styles.
 */
function tps_scripts() {

	global $wp_scripts;
	global $wp_query;

	$wp_scripts->add_data( 'jquery', 'group', 1 );
	$wp_scripts->add_data( 'jquery-core', 'group', 1 );
	$wp_scripts->add_data( 'jquery-migrate', 'group', 1 );

	//accesspress-social-login-lite plugin: move script to footer
	$wp_scripts->add_data( 'apsl-frontend-js', 'group', 1 );

	$page = $wp_query->get_queried_object();

	if ( ! empty( $page->post_name ) && TPS_CONTACT_PAGE == $page->post_name ) {
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}

		if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
			wpcf7_enqueue_styles();
		}
	}

//	wp_enqueue_style( 'tps-style', get_stylesheet_uri() );

//    wp_enqueue_style( 'tps-google-fonts', 'https://fonts.googleapis.com/css?family=Poppins:400,500' );

	wp_enqueue_style( 'tps-styles', get_template_directory_uri() . '/css/styles.css', array(), TPS_THEME_VERSION );

//    wp_enqueue_style( 'tps-owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css', array(), '2.2.0' );

	wp_enqueue_script( 'tps-bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js', array(), false, true );

	wp_enqueue_script( 'tps-bootstrap-hover-js', get_template_directory_uri() . '/js/bootstrap-hover-dropdown.js', array(), false, true );

	wp_enqueue_script( 'tps-scrolldown-js', get_template_directory_uri() . '/js/tps-scrolldown.js', array(), TPS_THEME_VERSION, true );

	wp_enqueue_script( 'tps-menu-search-js', get_template_directory_uri() . '/js/tps-menu-search.js', array(), TPS_THEME_VERSION, true );

	wp_enqueue_script( 'tps-scripts-js', get_template_directory_uri() . '/js/tps-scripts.js', array(), TPS_THEME_VERSION, true );

//	wp_enqueue_script( 'tps-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

//	wp_enqueue_script( 'tps-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/** dequeue font-awesome from social login plugin */
	wp_dequeue_style( 'fontawsome-css' );

}

add_action( 'wp_enqueue_scripts', 'tps_scripts' );