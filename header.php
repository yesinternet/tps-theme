<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tps
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php get_template_part('inc/custom-meta'); ?>

<?php wp_head(); ?>
</head>
<?php
global $wp_query;
$tps_page = $wp_query->get_queried_object();
$tps_post_name = !empty($tps_page->post_name) ? $tps_page->post_name : '';
$tps_body_background = '';
$tps_body_class = '';
if( !is_user_logged_in() && TPS_PROFILE_PAGE == $tps_post_name){
    $image_url = get_the_post_thumbnail_url('', 'tps_image_xlarge');
    if(!empty($image_url)) {
        $tps_body_background = "style=\"background-image: url('" . $image_url . "')\"";
        $tps_body_class = 'tps-body-background';
    }
}
?>
<body <?php body_class($tps_body_class); ?> <?php echo $tps_body_background;?>>
<?php do_action('tps-google-analytics'); ?>
<div id="page" class="site">
	<!--<a class="skip-link screen-reader-text" href="#content"><?php /*esc_html_e( 'Skip to content', 'tps' ); */?></a>-->

	<header id="masthead" class="site-header" role="banner">

		<nav id="site-navigation" class="tps-main-navigation navbar navbar-tps navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="fa fa-bars fa-lg fa-fw"></i>
                    </button>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="navbar-brand hidden-xs">
                        <img src="<?php header_image(); ?>" class="img-responsive" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
                    </a>
                    <div class="navbar-brand visible-xs-inline-block">
                    <?php echo get_custom_logo(); ?>
                    </div>
                    <div class="visible-xs-block">
                        <?php get_template_part( 'global/tps-menu-search' ); ?>
                    </div>
                </div>
                <div class="hidden-xs">
                    <?php get_template_part( 'global/tps-menu-search' ); ?>
                </div>
                <div class="collapse navbar-collapse navbar-right">
                    <?php wp_nav_menu(
                        array(
                            'theme_location'    => 'primary',
                            'menu_id'           => 'primary-menu',
                            'depth'             => 0,
    //                        'container'         => 'div',
    //                        'container_class'   => 'collapse navbar-collapse',
                            'menu_class' 		=> 'nav navbar-nav ',
                            'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
                            'walker' 			=> new wp_bootstrap_navwalker()
                        )
                    ); ?>
                </div>

            </div>

		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

