<?php

define('TPS_THEME_VERSION', '2.0.3');

/**
 * tps functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tps
 */

require get_template_directory() . '/config.php';

/**
 * Template setup
 */
require get_template_directory() . '/inc/template-setup.php';

/**
 * Register widget area.
 */
require get_template_directory() . '/inc/widgets-init.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue-scripts-styles.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/wp-bootstrap-navwalker.php';

/**
 * Load WooCommerce compatibility
 */
require get_template_directory() . '/inc/woocommerce.php';
require get_template_directory() . '/inc/tps-shortcodes.php';




