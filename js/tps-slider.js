
function tps_slider(element, settings) {
    element.owlCarousel(settings);
    element.on('loaded.owl.lazy', function (event) {
        var image_container = event.element.next('.tps-image-text-container');
        var image_text = image_container.children('.tps-image-text');
        var slider_spinner = image_container.children('.tps-slider-spinner');
        var target = event.target;

        slider_spinner.css({'display': 'none'});
        image_text.css({'display': 'block'});
    });
}