if (typeof jQuery === 'undefined') {
    throw new Error('TPS-ScrollDown JavaScript requires jQuery')
}

jQuery(function($) {
    $(".tps-scrollDown").click(function() {

        var parent = $(this).closest(".tps-image-full-height");
        var target = $(parent);
        var header_height = $(".tps-main-navigation").outerHeight() - 1;
        var offset = 0;

        if(target){
            offset = (target.offset().top + target.outerHeight()) - header_height;
        }

        if (offset > 0) {
            $("html,body").animate( { scrollTop: offset }, 1000 );
            return false;
        }

    });
});

jQuery(function($) {
    $(".tps-product-readmore a[href*='#']:not([href='#'])").click(function(event){
        tps_link_scrolldown(this, event);
    });

    $("a[href*='#']:not([href='#']).woocommerce-review-link").click(function(event){
        tps_link_scrolldown(this, event);
    });
});

function tps_link_scrolldown(el, event) {
    event.preventDefault();
    if (location.pathname.replace(/^\//,"") == el.pathname.replace(/^\//,"") && location.hostname == el.hostname)
    {
        var target = jQuery(el.hash);
        var header_height = jQuery(".tps-main-navigation").outerHeight() - 1;
        target = target.length ? target : jQuery("[name=" + el.hash.slice(1) +"]");

        if (target.length)
        {
            jQuery("html,body").animate( {scrollTop: target.offset().top - header_height}, 1000 );
            return false;
        }
    }
}