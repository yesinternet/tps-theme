if (typeof jQuery === 'undefined') {
    throw new Error('TPS-Menu-Search JavaScript requires jQuery');
}

jQuery(function($) {
    $(".tps-menu-search-btn").click(function(){
        $(this).next(".tps-menu-search-field").fadeToggle(300);
        $(this).next(".tps-menu-search-field").find(".search-field").focus();
        $(this).children(".fa").toggleClass("fa-search fa-close");
        $(this).toggleClass("open");
    });

    $(".navbar-header .tps-menu-search-btn").click(function(){
        $('.navbar-collapse.collapse.in').collapse('hide');
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $(".navbar-header .tps-menu-search-btn.open").click();
    });

    $(".tps-footer-search").click(function(event){
        event.preventDefault();
        $(".tps-menu-search-btn:visible").click();
    });
});
