jQuery(function ($) {
    $('[data-toggle="tooltip"]').tooltip();

    //Mobile menu burger icon change on click
    $('.navbar-collapse').on('show.bs.collapse', function () {
        $(".navbar-header .navbar-toggle .fa").toggleClass("fa-bars fa-close");
    });
    $('.navbar-collapse').on('hide.bs.collapse', function () {
        $(".navbar-header .navbar-toggle .fa").toggleClass("fa-bars fa-close");
    });

    //Single product readmore tab clicked when readmore link is clicked
    $('.tps-product-readmore a').click(function(){
        $( '.description_tab a' ).click();
    });
})