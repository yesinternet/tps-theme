if (typeof jQuery === 'undefined') {
    throw new Error('TPS-Register JavaScript requires jQuery')
}

jQuery(function($){
    $(".tps-register-btn").click(function (event) {
        event.preventDefault();
        var register = $(".tps-register-wrapper");
        var login = $(".tps-login-wrapper");
        var duration = 400;

        var date = new Date();
        date.setTime(date.getTime() + (10*365*24*60*60*1000));
        var expires = "expires=" + date.toUTCString() + ";";
        var tps_cookie = ';';

        if($(this).closest(".tps-register-wrapper").is(":visible")){
            register.slideToggle(duration, function(){
                login.slideToggle(duration);
            });
            tps_cookie = "tps_login_cookie=login;";
        }else{
            login.slideToggle(duration, function(){
                register.slideToggle(duration);
            });
            tps_cookie = "tps_login_cookie=register;";
        }

        document.cookie = tps_cookie + expires +" path=/";
    })

    var login_social_login_buttons = $(".tps-login-page form.login .apsl-login-networks");
    $(".tps-login-page form.login .woocommerce-LostPassword").before(login_social_login_buttons);

    var register_social_login_buttons = $(".tps-login-page form.register .apsl-login-networks");
    $(".tps-login-page form.register .woocommerce-Button").parent().after(register_social_login_buttons);

});