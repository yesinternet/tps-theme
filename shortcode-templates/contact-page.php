<?php

global $tps_shortcode_contact_atts;

$page_images        = $tps_shortcode_contact_atts['image_ids'];
$contact_form_id    = $tps_shortcode_contact_atts['id'];
$contact_form_title = $tps_shortcode_contact_atts['title'];
?>
<div class="row tps-section">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="text-center">
            <h2 class="text-capitalize"><?php _e( 'Give us a shout', 'tps' ); ?></h2>
            <p><?php _e( 'Have a burning question? Just want to chat with another human? Let us know what\'s on your mind.', 'tps' ); ?></p>
        </div>
    </div>
</div>

<?php if ( ! empty( $contact_form_id ) ): ?>
    <div class="row tps-section">
        <div class="col-sm-8 col-sm-offset-2">
            <span class="help-block"><?php _e( 'Fields marked with a * are required.', 'tps' ); ?></span>
			<?php echo do_shortcode( '[contact-form-7 id="' . $contact_form_id . '" title="' . $contact_form_title . '"]' ); ?>
        </div>
    </div>
<?php endif; ?>

<div class="row tps-section">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="text-center">
            <h2 class="text-capitalize"><?php _e( 'Join us on social media', 'tps' ); ?></h2>
            <h4 class="text-capitalize"><?php _e( 'Connect, follow and have a conversation with us', 'tps' ); ?></h4>
        </div>
    </div>
</div>

<div class="row tps-section">
    <div class="col-sm-8 col-sm-offset-2">
		<?php get_template_part( 'global/social-buttons' ); ?>
    </div>
</div>