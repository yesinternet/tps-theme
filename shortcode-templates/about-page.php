<?php

global $tps_shortcode_about_atts;
global $tps_our_team;

$page_images = $tps_shortcode_about_atts['image_ids'];
$our_team    = ! empty( $tps_our_team ) ? $tps_our_team : array();
?>

<div class="row tps-section">
    <div class="col-sm-12">
        <div class="text-center tps-about-header">
            <p><?php _e( 'We are a group of new parents with passion for making our lives (and others) easier and full of happy moments.', 'tps' ); ?></p>
            <p><?php _e( 'Therefore,  we have established a global network that searches the world for unique, usefull, innovative products', 'tps' ); ?>
				<?php _e( 'just for you and your kids. All in one place that you can save and buy! Now everything is at your fingertips.', 'tps' ); ?></p>
        </div>
    </div>
</div>

<div class="row tps-section">
    <div class="col-sm-12">
        <div class="text-center">
            <h2 class="tps-mot"><?php _e( 'Meet our team', 'tps' ); ?></h2>
            <p><?php _e( 'We\'re a global team of digital explorers, collaborative thinkers and innovative creators', 'tps' ); ?></p>
        </div>
    </div>
</div>

<div class="row tps-section">
	<?php foreach ( $our_team as $member ): ?>
        <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="tps-team-member-block">
				<?php $image = ! empty( $member['image_id'] ) ? wp_get_attachment_image( $member['image_id'], 'shop_catalog' ) : ''; ?>
				<?php $image = ! empty( $image ) ? $image : '<div class="tps-team-member-no-image"><i class="fa fa-user fa-4x"></i></div>'; ?>
				<?php $name = ! empty( $member['name'] ) ? $member['name'] : ''; ?>
				<?php $location = ! empty( $member['location'] ) ? $member['location'] : ''; ?>
				<?php $position = ! empty( $member['position'] ) ? $member['position'] : ''; ?>
				<?php $email = ( defined( 'TPS_OUR_TEAM_EMAIL' ) && ! empty( $name ) )
					? 'mailto:' . $name . TPS_OUR_TEAM_EMAIL . '?subject=' . __( 'Message from theparentsshop.com', 'tps' ) . '&body=' . __( 'Hello ', 'tps' ) . $name . ','
					: ''; ?>
                <div class="tps-team-member-image">
					<?php echo $image ?>
                    <div class="tps-team-member-image-overlay hidden-xs">
						<?php if ( ! empty( $email ) ): ?>
                            <a href="<?php echo $email; ?>" class="btn tps-btn"><i class="fa fa-envelope-o fa-fw"></i> <?php echo __( 'Contact ', 'tps' ) . $name; ?></a>
						<?php endif; ?>
                    </div>
                </div>
                <div class="tps-team-member-info">
                    <h5><?php echo $name; ?></h5>
                    <div><?php echo $location; ?></div>
                    <div class="tps-team-member-profession"><?php echo $position; ?></div>
					<?php if ( ! empty( $email ) ): ?>
                        <a href="<?php echo $email; ?>" class="visible-xs-block tps-contact-me"><?php echo __( 'Contact me', 'tps' ); ?></a>
					<?php endif; ?>
                </div>
            </div>
        </div>
	<?php endforeach; ?>
</div>