<?php

global $tps_faq_posts;

if ( empty( $tps_faq_posts ) ) {
	return '';
}

?>

<div class="panel-group tps-accordion tps-faq" id="accordion" role="tablist" aria-multiselectable="true">

	<?php

	foreach ( $tps_faq_posts as $post_id => $post ):

		?>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading-<?php echo $post_id; ?>">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $post_id; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $post_id; ?>">
						<?php echo $post->post_title; ?>
                    </a>
                </h4>
            </div>
            <div id="collapse-<?php echo $post_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $post_id; ?>">
                <div class="panel-body">
					<?php echo $post->post_content; ?>
                </div>
            </div>
        </div>

	<?php

	endforeach;

	?>

</div>
