<?php

global $tps_shortcode_press_atts;

$page_images = $tps_shortcode_press_atts['image_ids'];

$logos_link       = defined( 'TPS_DOWNLOADS_LOGOS' ) ? TPS_DOWNLOADS_LOGOS : '#';
$screenshots_link = defined( 'TPS_DOWNLOADS_SCREENSHOTS' ) ? TPS_DOWNLOADS_SCREENSHOTS : '#';
$documents_link   = defined( 'TPS_DOWNLOADS_DOCUMENTS' ) ? TPS_DOWNLOADS_DOCUMENTS : '#';

?>
<div class="row tps-section">
    <div class="col-sm-12">
        <p><?php _e( 'We love to see you there. We want to help you use our brand in the right way.', 'tps' ); ?></p>
    </div>
</div>

<div class="row tps-section">
    <div class="col-sm-12">
        <h4><?php _e( 'Logo Downloads', 'tps' ); ?></h4>
        <p><?php _e( 'Download the different versions of our logo. We ask you to respect our brand and not alter the logo in any way, shape or form.', 'tps' ); ?></p>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo $logos_link; ?>" title="Download logos">
            <div class="panel panel-default">
                <div class="tps-panel-body">
					<?php $image = ! empty( $page_images[0] ) ? wp_get_attachment_image( $page_images[0], 'tps_image_medium' ) : ''; ?>
					<?php echo $image; ?>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row tps-section">
    <div class="col-sm-12">
        <h4><?php _e( 'Screenshots', 'tps' ); ?></h4>
    </div>
    <div class="col-sm-3">
        <a href="<?php echo $screenshots_link; ?>" title="Download screenshots" target="_blank">
            <div class="panel panel-default">
                <div class="tps-panel-body">
					<?php $image = ! empty( $page_images[1] ) ? wp_get_attachment_image( $page_images[1], 'tps_image_medium' ) : ''; ?>
					<?php echo $image; ?>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row tps-section">
    <div class="col-sm-12">
        <h4><?php _e( 'Our story', 'tps' ); ?></h4>
        <a href="<?php echo $documents_link; ?>" title="Download documents"><?php _e( 'Download', 'tps' ); ?></a>
    </div>
</div>

<div class="row tps-section">
    <div class="col-sm-12">
    </div>
</div>