<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
global $woocommerce_loop;
global $tps_index;
global $tps_slider;
global $tps_shown_products;
global $tps_shortcode_name;


// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

//if( !empty( $tps_shown_products[$product->id] ) && $tps_shortcode_name == 'featured_products' ){
//    return;
//}

$is_fullheight        = $tps_slider['fullheight'] == 'true' ? true : false;
$is_lazyload          = $tps_slider['lazyload'] == 'true' ? true : false;
$lazyload_image_class = $is_lazyload ? 'owl-lazy' : '';
$lazyload_text_class  = $is_lazyload ? 'tps-slider-lazy' : '';

$post_id = $product->get_id();

$promo_heading = get_post_meta( $post_id, '_tps_promo_text', true );

$promo_image_id = get_post_meta( $post_id, '_tps_product_promo_image_id', true );

$image_size = $is_fullheight ? 'tps_image_xlarge' : 'tps_image_large';

if ( ! empty( $promo_image_id ) && ! empty( $promo_image_id ) ) {
	$image_atts['class'] = 'tps-slider-image ' . $lazyload_image_class;
	if ( $is_lazyload ) {
		$image_url = wp_get_attachment_image_url( $promo_image_id, $image_size );
		$image     = empty( $image_url ) ? '' : '<img src="' . $image_url . '" data-src="' . $image_url . '" class="' . $image_atts['class'] . '" />';
	} else {
		$image = wp_get_attachment_image( $promo_image_id, $image_size, false, $image_atts );
	}
}

if ( ! empty( $image ) ):

	$tps_shown_products[ $product->get_id() ] = $product;

	ob_start();
	?>
    <div class="tps-image-text-container <?php echo $lazyload_text_class; ?> ">
        <div class="tps-slider-spinner"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></div>
        <div class="tps-image-text">
			<?php if ( ! empty( $promo_heading ) ): ?>
                <div class="tps-image-promo-text"><?php echo $promo_heading; ?></div>
			<?php endif; ?>
			<?php woocommerce_template_single_add_to_cart(); ?>
        </div>
    </div>

	<?php

	$slide_text_container = ob_get_clean();

	$image = $image . $slide_text_container;

	$tps_index ++;
	?>


    <div class="woocommerce-LoopProduct-link tps-slider-image-container ">
		<?php echo $image; ?>
    </div>


<?php endif; ?>