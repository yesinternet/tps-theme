<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
global $tps_shown_products;
global $tps_shortcode_name;

if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

if ( ! empty( $tps_shown_products[ $product->get_id() ] ) && $tps_shortcode_name == 'featured_products' ) {
	return;
}

$tps_shown_products[ $product->get_id() ] = $product;

wc_get_template_part( 'content', 'product' );
