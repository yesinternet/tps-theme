<?php

global $tps_shortcode_sell_atts;

$page_images = $tps_shortcode_sell_atts['image_ids'];
//$image = !empty($page_images[0]) ? wp_get_attachment_image($page_images[0], 'tps_image_medium') : '';
?>

    <div class="tps-fullwidth-section tps-header-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center tps-header-block">
                        <h2><?php _e( 'The Parents Shop', 'tps' ); ?></h2>
                        <div><?php _e( 'is the world’s first hybrid product place that besides visitors offers you instant access directly up to 1.000.000 parents', 'tps' ); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tps-fullwidth-section tps-icons-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 text-center tps-icon-block">
                    <i class="fa fa-bullseye fa-3x"></i>
                    <h4><?php _e( 'Reach parents', 'tps' ); ?></h4>
					<?php _e( 'We find and promote curated products for parents that they will love to have', 'tps' ); ?>
                </div>
                <div class="col-sm-4 text-center tps-icon-block">
                    <i class="fa fa-line-chart fa-3x"></i>
                    <h4><?php _e( 'Boost sales & exposure', 'tps' ); ?></h4>
					<?php _e( 'Customers read about your product and go directly to your web page to buy', 'tps' ); ?>
                </div>
                <div class="col-sm-4 text-center tps-icon-block">
                    <i class="fa fa-euro fa-3x"></i>
                    <h4><?php _e( 'No commission fees', 'tps' ); ?></h4>
					<?php _e( 'Once you are listed in our platform, you stay permanent with no other hidden fees', 'tps' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 text-center tps-icon-block">
                    <i class="fa fa-clock-o fa-3x"></i>
                    <h4><?php _e( 'Go live in 24hours', 'tps' ); ?></h4>
					<?php _e( 'Your product is live for sale in 24 hours after successful submission', 'tps' ); ?>
                </div>
                <div class="col-sm-4 text-center tps-icon-block">
                    <i class="fa fa-envelope-o fa-3x"></i>
                    <h4><?php _e( 'Email marketing', 'tps' ); ?></h4>
					<?php _e( 'Our large database with parents ensures immediate sales enhancing also your brands awareness', 'tps' ); ?>
                </div>
                <div class="col-sm-4 text-center tps-icon-block">
                    <i class="fa fa-bullhorn fa-3x"></i>
                    <h4><?php _e( 'Free advertising', 'tps' ); ?></h4>
					<?php _e( 'Using SEO, Google adwords, advertising campaigns and PR we will continually promote your products', 'tps' ); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="tps-fullwidth-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3><?php _e( 'How it works', 'tps' ); ?></h3>
                    <div class="tps-small-separator"></div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="panel panel-default">
                        <span class="tps-panel-number">1</span>
                        <div class="tps-panel-body">
							<?php _e( 'Choose your favorite pricing plan', 'tps' ); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="panel panel-default">
                        <span class="tps-panel-number">2</span>
                        <div class="tps-panel-body">
							<?php _e( 'Proceed to payment and fill up the form', 'tps' ); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="panel panel-default">
                        <span class="tps-panel-number">3</span>
                        <div class="tps-panel-body">
							<?php _e( 'Approve your products page preview', 'tps' ); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="panel panel-default">
                        <span class="tps-panel-number">4</span>
                        <div class="tps-panel-body">
							<?php _e( 'Go live and boost your sales', 'tps' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tps-fullwidth-section tps-contact-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3><?php _e( 'Want more info?', 'tps' ); ?></h3>
                    <div class="tps-sub-header"><span><?php _e( 'Just talk to us now?', 'tps' ); ?></span></div>
                    <a href="<?php echo home_url(); ?>/contact" class="btn btn-default btn-lg"><?php _e( 'Ι want more info', 'tps' ); ?></a>
                </div>
            </div>
        </div>
    </div>

<?php

global $tps_pricing_plan_ids;

if ( ! empty( $tps_pricing_plan_ids ) && is_array( $tps_pricing_plan_ids ) ):
	?>

    <div class="tps-fullwidth-section tps-pricing-section">
        <div class="container">
            <div class="row">
				<?php foreach ( $tps_pricing_plan_ids as $id ): ?>
					<?php $pricing_plan = wc_get_product( $id ); ?>
					<?php if ( $pricing_plan == false ) {
						continue;
					} ?>
                    <div class="col-sm-4 col-md-4 text-center">
                        <div class="tps-pricing-plan <?php echo $pricing_plan->is_featured() ? 'tps-pricing-popular' : ''; ?>">
                            <div class="tps-pricing-head">
                                <div class="name"><?php echo $pricing_plan->get_title(); ?></div>
                                <div class="price">
									<?php echo $pricing_plan->get_price_html(); ?>
                                </div>
								<?php if ( $pricing_plan->is_featured() ): ?>
                                    <div class="badge">
										<?php _e( 'Popular', 'tps' ); ?>
                                    </div>
								<?php endif; ?>
                            </div>
                            <div class="tps-pricing-body">
								<?php echo $pricing_plan->get_short_description(); ?>
                            </div>
                            <div class="tps-pricing-footer">
                                <a href="<?php echo home_url() . $pricing_plan->add_to_cart_url(); ?>" class="btn btn-default"><?php _e( 'Buy now', 'tps' ); ?></a>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </div>

<?php endif; ?>