<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tps
 */

get_header(); ?>

    <div id="primary" class="content-area archive-wrapper">
        <main id="main" class="site-main container" role="main">
            <div class="row">
                <div class="col-sm-12">
                    <?php do_shortcode('[tps_posts_categories]'); ?>
                    <?php //do_shortcode('[tps_posts_categories secondary=1]'); ?>
                </div>
                <?php
                if ( have_posts() ) : ?>

                    <div class="col-sm-12">
                        <header>
                            <?php
                                the_archive_title( '<h1 class="page-title">', '</h1>' );
                                the_archive_description( '<div class="archive-description">', '</div>' );
                            ?>
                        </header><!-- .page-header -->
                    </div>

                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post();
                    ?>
                        <div class="col-xs-6 col-sm-4">
                    <?php

                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part( 'template-parts/content', get_post_format() );

                    ?>
                        </div>
                    <?php
                    endwhile;

                    echo '<div class="col-xs-12">';
                    get_template_part( 'global/tps-pagination-blog' );
                    echo '</div>';

                else :

                    echo '<div class="col-xs-12">';
                    get_template_part( 'template-parts/content', 'none' );
                    echo '</div>';

                endif; ?>
            </div><!-- .row -->
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
