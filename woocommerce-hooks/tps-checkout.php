<?php
/**
 * Remove woocommerce actions
 */


/**
 * Add tps actions
 */

add_filter( 'woocommerce_form_field_args', 'tps_woocommerce_form_field_args', 10, 3 );
add_filter( 'woocommerce_order_button_html', 'tps_woocommerce_order_button_html' );
add_filter( 'woocommerce_pay_order_button_html', 'tps_woocommerce_pay_order_button_html' );


/**
 * Action functions
 */

function tps_woocommerce_form_field_args( $args, $key, $value ) {
	$args['class'][] = 'form-group form-group-lg';
	if ( ! empty( $args['type'] ) && $args['type'] != 'country' ) {
		$args['input_class'][] = 'form-control';
	}
	if ( ! empty( $args['type'] ) && $args['type'] == 'textarea' && ! empty( $args['id'] ) && $args['id'] == 'order_comments' ) {
		$args['custom_attributes']['rows'] = 8;
	}

	return $args;
}

function tps_woocommerce_order_button_html() {
	global $order_button_text;
	$button = '<button type="submit" class="button alt btn btn-primary btn-lg btn-block" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>';
	$cards  = '<div class="tps-checkout-cards"><i class="fa fa-cc-visa fa-2x"></i> <i class="fa fa-cc-mastercard fa-2x"></i> <i class="fa fa-cc-amex fa-2x"></i></div>';
	$return = $button . $cards;

	return $return;
}

function tps_woocommerce_pay_order_button_html() {
	global $order_button_text;
	$button = '<button type="submit" class="button alt btn btn-primary btn-lg btn-block" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>';
	$cards  = '<div class="tps-checkout-cards"><i class="fa fa-cc-visa fa-2x"></i> <i class="fa fa-cc-mastercard fa-2x"></i> <i class="fa fa-cc-amex fa-2x"></i></div>';
	$return = $button . $cards;

	return $return;
}