<?php

/**
 * Remove woocommerce actions
 */

remove_action( 'woocommerce_account_navigation', 'woocommerce_account_navigation' );
remove_action( 'woocommerce_account_content', 'woocommerce_account_content' );


/**
 * Add tps actions
 */

add_action( 'woocommerce_account_navigation', 'tps_woocommerce_account_navigation' );
add_action( 'woocommerce_before_account_navigation', 'tps_woocommerce_before_account_navigation' );
add_filter( 'woocommerce_account_menu_item_classes', 'tps_woocommerce_account_menu_item_classes', 10, 2 );
add_action( 'woocommerce_account_content', 'tps_woocommerce_account_content' );

add_filter( 'woocommerce_registration_redirect', 'tps_woocommerce_registration_redirect' );


/**
 * Action functions
 */


function tps_woocommerce_account_navigation() {
	wc_get_template( 'myaccount/navigation.php' );
}

function tps_woocommerce_before_account_navigation() {

	$current_user = wp_get_current_user();

	$email = $current_user->user_email;

	echo '<div class="tps-my-account-avatar">';
	echo get_avatar( $email, 150 );
	echo '</div>';
}

function tps_woocommerce_account_menu_item_classes( $classes, $endpoint ) {
	$classes[] = "list-group-item";

	return $classes;
}

function tps_woocommerce_account_content() {
	global $wp;

	the_title( '<h1 class="page-title">', '</h1>' );

	foreach ( $wp->query_vars as $key => $value ) {
		// Ignore pagename param.
		if ( 'pagename' === $key ) {
			continue;
		}

		if ( has_action( 'woocommerce_account_' . $key . '_endpoint' ) ) {
			do_action( 'woocommerce_account_' . $key . '_endpoint', $value );

			return;
		}
	}

	// No endpoint found? Default to dashboard.
	wc_get_template( 'myaccount/dashboard.php', array(
		'current_user' => get_user_by( 'id', get_current_user_id() ),
	) );
}

function tps_woocommerce_registration_redirect( $redirect ) {
	if ( ! empty( $_POST['redirect'] ) ) {
		$redirect = $_POST['redirect'];
	} elseif ( wp_get_referer() ) {
		$redirect = wp_get_referer();
	} else {
		$redirect = wc_get_page_permalink( 'myaccount' );
	}

	return $redirect;
}