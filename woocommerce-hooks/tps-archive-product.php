<?php


/**
 * Remove woocommerce actions
 */

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * Add tps actions
 */


add_action( 'woocommerce_before_main_content', 'tps_woocommerce_category_page_start', 11 );
add_action( 'woocommerce_before_main_content', 'tps_woocommerce_category_image', 11 );

add_filter( 'woocommerce_show_page_title', 'tps_woocommerce_remove_category_header' );

add_action( 'woocommerce_archive_description', 'tps_woocommerce_product_category_description_before', 9 );
add_action( 'woocommerce_archive_description', 'tps_woocommerce_product_category_description_after', 11 );

//add_action( 'woocommerce_before_shop_loop', 'tps_woocommerce_before_category_content_start', 19 );
//add_action( 'woocommerce_before_shop_loop', 'tps_woocommerce_before_category_content_end', 30 );
add_action( 'woocommerce_before_shop_loop', 'tps_woocommerce_before_shop_loop', 30 );

add_action( 'woocommerce_after_shop_loop', 'tps_woocommerce_after_shop_loop', 11 );

add_action( 'woocommerce_after_main_content', 'tps_woocommerce_category_page_end', 9 );

/**
 * Action functions
 */

function tps_woocommerce_category_page_start() {

}

function tps_woocommerce_category_image() {
	if ( is_product_category() ) {
		global $wp_query;
		$cat           = $wp_query->get_queried_object();
		$thumbnail_id  = get_term_meta( $cat->term_id, 'thumbnail_id', true );
		$image         = wp_get_attachment_image( $thumbnail_id, 'tps_image_xlarge' );
		$promo_heading = get_term_meta( $cat->term_id, '_tps_term_meta_product_cat_promo_text', true );
		if ( $image ) {
			echo '<div class="tps-product-category-image-container tps-image-medium-height">';
			echo $image;
			echo '    <div class="tps-image-text-container">';
			echo '        <div class="tps-image-text">';
			echo '            <div class="page-title tps-image-title"><span>' . woocommerce_page_title( false ) . '</span></div>';
			if ( ! empty( $promo_heading ) ) {
				echo '        <div class="tps-image-promo-text">' . $promo_heading . '</div>';
			}
			echo '        </div>';
			echo '    </div>';
			echo '</div>';
		}
	}
}

function tps_woocommerce_remove_category_header() {
	return false;
}

function tps_woocommerce_product_category_description_before() {
	if ( is_product_category() ) {
		echo '<div class="tps-product-cat-description container">';
	}
}

function tps_woocommerce_product_category_description_after() {
	if ( is_product_category() ) {
		echo '</div>';
	}
}

function tps_woocommerce_before_category_content_start() {
	//get_template_part( 'global/tps-breadcrumbs' );

	echo '            <div class="container">';
	echo '                <div class="row">';
	echo '                    <div class="col-sm-12">';
	echo '                        <div class="tps-product-category-before-grid">';
}

function tps_woocommerce_before_category_content_end() {
	echo '                        </div>';
	echo '                    </div><!-- col-sm-12 -->';
	echo '                </div><!-- row -->';
	echo '            </div><!-- container -->';
}

function tps_woocommerce_before_shop_loop() {
	echo tps_woocommerce_archive_product_recommended_grid();
	echo tps_woocommerce_archive_product_category_slider();

	echo '            <div class="container">';
	echo '<h3 class="tps-shortcode-grid-header text-center">' . __( 'Latest Products', 'tps' ) . '</h3>';
	woocommerce_catalog_ordering();
}

function tps_woocommerce_after_shop_loop() {
	echo '            </div><!-- container -->';
}

function tps_woocommerce_category_page_end() {

}


/* block functions */

function tps_woocommerce_archive_product_recommended_grid() {
	if ( is_search() ) {
		return '';
	}
	$slug   = tps_woocommerce_get_category_slug();
	$return = do_shortcode( '[tps_featured_products columns="3" per_page="3" category=' . $slug . ' exclude_products=]' );

	return $return;
}

function tps_woocommerce_archive_product_category_slider() {
	$slug   = tps_woocommerce_get_category_slug();
	$return = do_shortcode( '[tps_product_cat_products_slider category=' . $slug . ' fullwidth=true fullheight=true]' );

	return $return;
}

if ( ! function_exists( 'tps_woocommerce_get_category_slug' ) ) {
	function tps_woocommerce_get_category_slug() {
		global $wp_query;
		$cat = $wp_query->get_queried_object();

		return ! empty( $cat->slug ) ? $cat->slug : '';
	}
}