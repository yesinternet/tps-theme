<?php

require get_template_directory() . '/woocommerce-hooks/tps-global.php';
require get_template_directory() . '/woocommerce-hooks/tps-loop.php';
require get_template_directory() . '/woocommerce-hooks/tps-myaccount.php';
require get_template_directory() . '/woocommerce-hooks/tps-checkout.php';


require get_template_directory() . '/woocommerce-hooks/tps-archive-product.php';
require get_template_directory() . '/woocommerce-hooks/tps-content-product.php';
require get_template_directory() . '/woocommerce-hooks/tps-content-single-product.php';