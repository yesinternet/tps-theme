<?php


/**
 * Remove woocommerce actions
 */

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
//remove_action( 'woocommerce_after_shop_loop_item_title', 'tps_woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );


/**
 * Add tps actions
 */

add_action( 'woocommerce_before_shop_loop_item', 'tps_before_shop_loop_item_container_open', 9 );

add_action( 'woocommerce_before_shop_loop_item_title', 'tps_product_image_overlay', 15 );

add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 15 );

add_action( 'woocommerce_before_shop_loop_item_title', 'tps_product_icons', 16 );

//add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 20 );
add_action( 'woocommerce_before_shop_loop_item_title', 'tps_product_info_container_open', 20 );

add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 9 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 11 );

add_action( 'woocommerce_after_shop_loop_item_title', 'tps_woocommerce_template_single_meta', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'tps_product_info_container_close', 20 );

add_action( 'woocommerce_after_shop_loop_item', 'tps_after_shop_loop_item_container_close', 5 );


/**
 * Action functions
 */

function tps_before_shop_loop_item_container_open() {
	echo '<div class="tps-product-container">';
	echo '    <div class="tps-product-image">';
}

function tps_product_image_overlay() {
	echo '<div class="tps-star-rating">';
	wc_get_template( 'loop/rating.php' );
	echo '</div>';

	echo '<div class="tps-overlay"></div>';
}

function tps_product_icons() {
	get_template_part( "global/product-properties-icons" );
	echo '    </div>';
}

function tps_product_info_container_open() {
	echo '<div class="tps-product-info">';
}

function tps_woocommerce_template_single_meta() {
	global $product;

	echo '<div class="tps-categories">' . wc_get_product_category_list( $product->get_id(), ', ', '', '' ) . '</div>';
}

function tps_product_info_container_close() {
	echo '</div>';
	get_template_part( "global/product-grid-buttons" );
}

function tps_after_shop_loop_item_container_close() {
	echo '</div>';
}
