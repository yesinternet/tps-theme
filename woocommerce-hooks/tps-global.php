<?php

/**
 * Remove woocommerce actions
 */

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );


/**
 * Add tps actions
 */

add_filter( 'woocommerce_get_breadcrumb', '__return_false' );
add_action( 'woocommerce_before_main_content', 'tps_woocommerce_output_content_wrapper', 10 );
add_action( 'woocommerce_after_main_content', 'tps_woocommerce_output_content_wrapper_end', 10 );


/**
 * Action functions
 */

function tps_woocommerce_output_content_wrapper() {
	wc_get_template( 'global/wrapper-start.php' );
}

function tps_woocommerce_output_content_wrapper_end() {
	wc_get_template( 'global/wrapper-end.php' );
}
