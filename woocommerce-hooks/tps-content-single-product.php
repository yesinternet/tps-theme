<?php


/**
 * Remove woocommerce actions
 */

remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


/**
 * Add tps actions
 */

add_action( 'woocommerce_before_single_product_summary', 'tps_single_product_top_start', 5 );
add_action( 'woocommerce_before_single_product_summary', 'tps_single_product_after_images', 21 );
add_action( 'woocommerce_single_product_summary', 'tps_woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_external_add_to_cart', 'tps_woocommerce_external_add_to_cart', 10 );
add_action( 'woocommerce_single_product_summary', 'tps_woocommerce_template_single_sharing', 39 );
add_action( 'woocommerce_single_product_summary', 'tps_woocommerce_template_single_product_icons', 39 );
add_action( 'woocommerce_after_single_product_summary', 'tps_single_product_top_end', 5 );

add_action( 'woocommerce_after_single_product_summary', 'tps_single_product_bottom_start', 5 );
add_action( 'woocommerce_after_single_product_summary', 'tps_woocommerce_output_product_data_tabs', 10 );
add_filter( 'woocommerce_product_description_heading', 'tps_woocommerce_product_description_heading' );
add_action( 'woocommerce_after_single_product_summary', 'tps_single_product_bottom_end', 100 );
add_action( 'woocommerce_after_single_product_summary', 'tps_single_product_related_products', 101 );

add_filter( 'gettext', 'tps_tab_reviews_text', 10, 3 );

/**
 * Action functions
 */

function tps_single_product_top_start() {
	echo '<div class="tps-product-top row">';
	echo '    <div class="container">';
	echo '        <div class="row">';
	echo '            <div class="tps-product-images col-sm-6">';
}

function tps_single_product_after_images() {
	echo '            </div>';
	echo '            <div class="tps-product-summary col-sm-6">';
}

function tps_woocommerce_template_single_excerpt() {
	wc_get_template( 'single-product/short-description.php' );
}

function tps_woocommerce_external_add_to_cart() {
	global $product;

	if ( ! $product->add_to_cart_url() ) {
		return;
	}
	echo '<div class="tps-single-product-buttons">';
	wc_get_template( 'single-product/add-to-cart/external.php', array(
		'product_url' => $product->add_to_cart_url(),
		'button_text' => $product->single_add_to_cart_text()
	) );
	if ( is_single() ) {
		do_action( 'tps-single-product-wishlist-button' );
	}
	echo '</div>';
}

function tps_woocommerce_template_single_sharing() {
	wc_get_template( 'single-product/share.php' );
}

function tps_woocommerce_template_single_product_icons() {
	get_template_part( "global/product-properties-icons" );
}

function tps_single_product_top_end() {
	echo '            </div><!-- .tps-product-summary -->';
	echo '        </div><!-- .row -->';
	echo '    </div><!-- .container -->';
	echo '</div><!-- .tps-product-top -->';
}

function tps_single_product_bottom_start() {
	echo '<div class="tps-product-bottom row">';
	echo '    <div class="container">';
	echo '        <div class="row">';
	echo '            <div class="col-sm-8">';
}

function tps_woocommerce_output_product_data_tabs() {
	wc_get_template( 'single-product/tabs/tabs.php' );
}

function tps_woocommerce_product_description_heading() {
	return '';
}

function tps_single_product_bottom_end() {
	echo '            </div>';
	get_sidebar( "shop" );
	echo '        </div><!-- .row -->';
	echo '    </div><!-- .container -->';
	echo '</div><!-- .tps-product-bottom -->';
}

function tps_single_product_related_products() {
	global $product;

	$categories = array();
	$terms      = get_the_terms( $product->get_id(), 'product_cat' );

	if ( empty( $terms ) ) {
		return '';
	}

	foreach ( $terms as $key => $category ) {
		$categories[ $key ] = $category->slug;
	}

	$categories = implode( ',', $categories );

	echo '<div class="row tps-section">';
	echo '<h3 class="text-center">' . __( 'More from this category', 'tps' ) . '</h3>';
	echo do_shortcode( '[tps_product_category category="' . $categories . '" per_page=6 columns=3]' );
	echo '</div><!-- .tps-section -->';
}

function tps_tab_reviews_text( $translation, $text, $domain ) {

	if ( $domain == 'woocommerce' && $text == 'Reviews (%d)' ) {
		$translation = 'Reviews';
	}

	return $translation;
}