<?php

/**
 * Remove woocommerce actions
 */

remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );


/**
 * Add tps actions
 */

add_action( 'woocommerce_after_shop_loop', 'tps_woocommerce_pagination', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'tps_woocommerce_template_loop_price', 10 );


/**
 * Action functions
 */

function tps_woocommerce_pagination() {
	wc_get_template( 'loop/pagination.php' );
}

function tps_woocommerce_template_loop_price() {
	wc_get_template( 'loop/price.php' );
}
