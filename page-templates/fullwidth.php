<?php
/**
 * Template Name: Full width page template
 */

get_header(); ?>

    <div id="primary" class="content-area page-wrapper tps-fullwidth">

        <main id="main" class="site-main " role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'fullwidth' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php

get_footer();