<?php
/**
 * Template Name: My Account page template
 */

get_header(); ?>

    <div id="primary" class="content-area page-wrapper tps-myaccount">
        <main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'myaccount' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
$footer = ! is_user_logged_in() ? "login" : "";
get_footer( $footer );