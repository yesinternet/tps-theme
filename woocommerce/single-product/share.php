<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/share.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$tps_post_id = $product->get_id();

$tps_product_url = urlencode( get_the_permalink() );

$tps_product_image_url = urlencode( get_the_post_thumbnail_url( get_post( $product->get_id() ), 'shop_catalog' ) );

$tps_promo_heading                 = get_post_meta( $tps_post_id, '_tps_promo_text', true );
$tps_product_pinterest_description = ! empty( $tps_promo_heading ) ? $tps_promo_heading : apply_filters( 'woocommerce_short_description', $product->get_short_description() );
$tps_product_pinterest_description = urlencode( wp_strip_all_tags( $tps_product_pinterest_description, true ) );
$tps_product_pinterest_url         = "//pinterest.com/pin/create/button/?url=" . $tps_product_url . "&media=" . $tps_product_image_url . "&description=" . $tps_product_pinterest_description;

?>

<?php do_action( 'woocommerce_share' ); // Sharing plugins can hook into here ?>
<div class="tps-product-share">
    <a href="//www.facebook.com/sharer.php?u=<?php echo $tps_product_url; ?>" class="tps-btn-social tps-btn-facebook" target="_blank" title="<?php _e( 'Share it on Facebook', 'tps' ); ?>">
        <i class="fa fa-facebook fa-fw fa-lg"></i>
    </a>
    <a href="//twitter.com/home?status=<?php echo $tps_product_url; ?>" class="tps-btn-social tps-btn-twitter" target="_blank" title="<?php _e( 'Share it on Twitter', 'tps' ); ?>">
        <i class="fa fa-twitter fa-fw fa-lg"></i>
    </a>
    <a href="<?php echo $tps_product_pinterest_url; ?>" class="tps-btn-social tps-btn-pinterest" target="_blank" title="<?php _e( 'Share it on Pinterest', 'tps' ); ?>">
        <i class="fa fa-pinterest fa-fw fa-lg"></i>
    </a>
    <a href="//plus.google.com/share?url=<?php echo $tps_product_url; ?>" class="tps-btn-social tps-btn-google-plus" target="_blank" title="<?php _e( 'Share it on Google Plus', 'tps' ); ?>">
        <i class="fa fa-google-plus fa-fw fa-lg"></i>
    </a>
    <a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo $tps_product_url; ?>" class="tps-btn-social tps-btn-email" title="<?php _e( 'Share it with Email', 'tps' ); ?>">
        <i class="fa fa-envelope-o fa-fw fa-lg"></i>
    </a>
</div>
