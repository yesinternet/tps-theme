<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php $tps_login_cookie = ! empty( $_COOKIE['tps_login_cookie'] ) ? $_COOKIE['tps_login_cookie'] : 'register'; ?>

<?php $tps_login_style = ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' && $tps_login_cookie == 'register' ) ? 'style="display: none;"' : ''; ?>

<div class="row tps-login-page" id="customer_login">

    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 tps-login-wrapper" <?php echo $tps_login_style; ?> >

        <h1 class="h1 text-center"><?php esc_html_e( 'Sign In', 'tps' ); ?></h1>
        <div class="tps-sub-header text-center">
            <span><?php esc_html_e( "Welcome back!", "tps" ); ?></span>
            <span><?php esc_html_e( 'Check out new innovative products that you will love to have', 'tps' ); ?></span>
        </div>

        <form method="post" class="login">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

            <div class="woocommerce-FormRow woocommerce-FormRow--wide form-group">
                <label for="username"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) {
					echo esc_attr( $_POST['username'] );
				} ?>"/>
            </div>
            <div class="woocommerce-FormRow woocommerce-FormRow--wide form-group">
                <label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                <input class="woocommerce-Input woocommerce-Input--text input-text form-control" type="password" name="password" id="password"/>
            </div>

			<?php do_action( 'woocommerce_login_form' ); ?>

            <div class="form-group">
				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                <div class="checkbox">
                    <label for="rememberme" class="inline">
                        <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever"/> <?php esc_html_e( 'Remember me', 'woocommerce' ); ?>
                    </label>
                </div>
                <button type="submit" class="woocommerce-Button button btn btn-primary btn-block" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
            </div>
            <div class="woocommerce-LostPassword lost_password text-center">
                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
            </div>
            <input type="hidden" name="redirect" value="<?php echo wp_get_referer(); ?>"/>
			<?php do_action( 'woocommerce_login_form_end' ); ?>

        </form>

		<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
            <a href="#" class="tps-register-btn">
                <div class="text-center tps-new-account">
                    <div class="text-uppercase tps-register-btn-primary"><?php esc_html_e( "Create a free account", "tps" ) ?></div>
                    <div><?php esc_html_e( 'and discover the products of your dreams', 'tps' ); ?></div>
                </div>
            </a>
		<?php endif; ?>

    </div>

	<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 tps-register-wrapper" <?php if ( $tps_login_cookie == 'login' ) {
			echo 'style="display:none;"';
		} ?>>

            <h2 class="h1 text-center"><?php esc_html_e( 'Hello there!', 'tps' ); ?></h2>
            <div class="tps-sub-header text-center">
                <span><?php esc_html_e( "It's nice to see that you're interested.", "tps" ); ?></span>
                <span><?php esc_html_e( 'Sign up and be the first to know about new, innovative products for parents in your inbox!', 'tps' ); ?></span>
            </div>

            <form method="post" class="register">

				<?php do_action( 'woocommerce_register_form_start' ); ?>

				<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                    <div class="woocommerce-FormRow woocommerce-FormRow--wide form-group">
                        <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) {
							echo esc_attr( $_POST['username'] );
						} ?>"/>
                    </div>

				<?php endif; ?>

                <div class="woocommerce-FormRow woocommerce-FormRow--wide form-group">
                    <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) {
						echo esc_attr( $_POST['email'] );
					} ?>"/>
                </div>

				<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                    <div class="woocommerce-FormRow woocommerce-FormRow--wide form-group">
                        <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="password" id="reg_password"/>
                    </div>

				<?php endif; ?>

                <!-- Spam Trap -->
                <div style="<?php echo( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php esc_html_e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap"
                                                                                                                                                                                               tabindex="-1" autocomplete="off"/></div>

				<?php do_action( 'woocommerce_register_form' ); ?>
				<?php do_action( 'register_form' ); ?>

                <div class="woocomerce-FormRow form-group">
					<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                    <button type="submit" class="woocommerce-Button button btn btn-primary btn-block" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
                </div>

                <input type="hidden" name="redirect" value="<?php echo wp_get_referer(); ?>"/>

				<?php do_action( 'woocommerce_register_form_end' ); ?>

            </form>

            <a href="#" class="tps-register-btn">
                <div class="text-center tps-already-account">
                    <div><?php esc_html_e( "Already have an account?", "tps" ) ?></div>
                    <div class="text-uppercase tps-register-btn-primary"><?php esc_html_e( "Sign in", "tps" ); ?></div>
                </div>
            </a>

        </div>

		<?php wp_enqueue_script( 'tps-register-js', get_template_directory_uri() . '/js/tps-register.js', array(), TPS_THEME_VERSION, true ); ?>
	<?php endif; ?>

</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
