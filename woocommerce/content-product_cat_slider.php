<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $tps_index;
global $tps_slider;

$is_fullheight        = $tps_slider['fullheight'] == 'true' ? true : false;
$is_lazyload          = $tps_slider['lazyload'] == 'true' ? true : false;
$lazyload_image_class = $is_lazyload ? 'owl-lazy' : '';
$lazyload_text_class  = $is_lazyload ? 'tps-slider-lazy' : '';

$category_name = $category->name;
$promo_heading = get_term_meta( $category->term_id, '_tps_term_meta_product_cat_promo_text', true );

$thumbnail_id = get_term_meta( $category->term_id, 'thumbnail_id', true );

$image_size = $is_fullheight ? 'tps_image_xlarge' : 'tps_image_large';

if ( $thumbnail_id ) {
	$image_atts['class'] = 'tps-slider-image ' . $lazyload_image_class;
	if ( $is_lazyload ) {
		$image_url = wp_get_attachment_image_url( $thumbnail_id, $image_size );
		$image     = empty( $image_url ) ? '' : '<img src="' . $image_url . '" data-src="' . $image_url . '" class="' . $image_atts['class'] . '" />';
	} else {
		$image = wp_get_attachment_image( $thumbnail_id, $image_size, false, $image_atts );
	}
}

if ( ! empty( $image ) ) :

	ob_start();
	?>
    <div class="tps-image-text-container <?php echo $lazyload_text_class; ?> ">
        <div class="tps-slider-spinner"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></div>
        <div class="tps-image-text">
			<?php echo '<div class="tps-image-title"><span>' . $category_name . '</span></div>'; ?>
			<?php if ( ! empty( $promo_heading ) ): ?>
                <div class="tps-image-promo-text"><?php echo $promo_heading; ?></div>
			<?php endif; ?>
			<?php if ( ! empty( $tps_slider['slider_button_text'] ) ): ?>
                <a href="<?php echo get_term_link( $category, 'product_cat' ); ?>" class="btn btn-primary" title="<?php _e( 'Go to Category', 'tps' ); ?>">
					<?php echo $tps_slider['slider_button_text']; ?>
                </a>
			<?php endif; ?>
        </div>
    </div>

	<?php

	$slide_text_container = ob_get_clean();

	$image = $image . $slide_text_container;

	$tps_index ++;

	?>

    <div class="tps-slider-image-container ">
		<?php echo $image; ?>
    </div>

<?php endif; ?>